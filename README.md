SUMMARY
=======
This is the DuMuX module containing the code for producing the results
submitted for:

S. Kiemle<br>
Modelling of Isotope Transport and Evaporation Rates at the Porous Medium Free-Flow Interface
<br>
Master's Thesis, 2019<br>
Universität Stuttgart

Installation
============

The easiest way to install this module is to create a new directory and clone this module:
```
mkdir Kiemle2019a && cd Kiemle2019a
git clone https://git.iws.uni-stuttgart.de/dumux-pub/Kiemle2019a.git
```

After that, execute the file [installKiemle2019a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Kiemle2019a/raw/master/installKiemle2019a.sh).

```
./Kiemle2019a/installKiemle2019a.sh
```

This should automatically download all necessary modules and check out the correct versions.

Used Versions and Software
==========================

For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installKiemle2019a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Kiemle2019a/raw/master/installKiemle2019a.sh).


Installation with Docker
========================

Create a new folder in your favourite location and change into it

```bash
mkdir Kiemle2019a
cd Kiemle2019a
```

Download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-pub/kiemle2019a/-/raw/master/docker_kiemle2019a.sh
```

Open the Docker Container
```bash
bash docker_kiemle2019a.sh open
```

After the script has run successfully, you may build all executables

```bash
cd kiemle2019a/build-cmake
make build_tests
```

and you can run them individually. They are located in the build-cmake folder according to the following paths:

- test/soilcolumn

It can be executed with an input file, e.g.

```
cd test/soilcolumn
./soilcolumn_isotope params.input
```
