// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup Binarycoefficients
 * \brief Binary coefficients for heavy water and air.
 */
#ifndef DUMUX_BINARY_COEFF_AIR_H2018_HH
#define DUMUX_BINARY_COEFF_AIR_H2018_HH

#include <cmath>
#include <dumux/material/binarycoefficients/fullermethod.hh>
#include "h2o18.hh"
#include <dumux/material/components/air.hh>

namespace Dumux {
    namespace BinaryCoeff {
        
        /*! //TODO: Prüfe, welche Näherung für gasDiffCoeff und Henry sich besser eigenet. 
         * \ingroup Binarycoefficients
         * \brief Binary coefficients for water and air.
         */
        class Air_H2O18
        {
        public:
            /*!
             * \brief Henry coefficient \f$\mathrm{[Pa]}\f$  for air in liquid water.
             * \param temperature the temperature \f$\mathrm{[K]}\f$
             *
             * Henry coefficient See:
             * Stefan Finsterle (1993, page 33 Formula (2.9)) \cite finsterle1993 <BR>
             * (fitted to data from Tchobanoglous & Schroeder, 1985 \cite tchobanoglous1985 )
             */
            
            //TODO: Welcher Henry coefficient? 
            template <class Scalar>
            static Scalar henry(Scalar temperature)
            {      using std::exp;
                Scalar r = (0.8942+1.47*exp(-0.04394*(temperature-273.15)))*1.E-10; // Herleitung: sonst Wasser - H2O18 ähnlich, daher Werte sollten ähnlich sein. Nach IAPWS Methode nur auf Wasser bezogen --> Alternativ? 
                
                return 1./r;
                
                
                //Alternativ: nach Sanders (Keine genauen Werte für H2O18 und Luft vorhanden) 
                //         // after Sander
                //         Scalar sanderH = 6.5e-4;    //[M/atm] //Wert für N2 in Wasser! Quelle: Sander (1999) 
                //         //conversion to our Henry definition
                //         Scalar dumuxH = sanderH / 101.325; // has now [(mol/m^3)/Pa]
                //         dumuxH *= 20.00e-6;  //multiplied by molar volume of reference phase = H2O18 (!Mit Dichte von Wasser umgerechnet! (1000kg/m³)) 
                //         return 1.0/dumuxH; // [Pa]
                //       
            }
            
            /*!
             * \brief Binary diffusion coefficient \f$\mathrm{[m^2/s]}\f$ for molecular water and air
             *
             * \param temperature the temperature \f$\mathrm{[K]}\f$
             * \param pressure the phase pressure \f$\mathrm{[Pa]}\f$
             * Vargaftik: Tables on the thermophysical properties of liquids and gases.
             * John Wiley & Sons, New York, 1975. \cite vargaftik1975 <BR>
             * Walker, Sabey, Hampton: Studies of heat transfer and water migration in soils.
             * Dep. of Agricultural and Chemical Engineering, Colorado State University,
             * Fort Collins, 1981. \cite walker1981
             */
            template <class Scalar>
            static Scalar gasDiffCoeff(Scalar temperature, Scalar pressure)
            {
                
                //TODO: Prüfe, welche Methode besser ist! 
                
                //über Fuller Methode 
                
//                          typedef Dumux::Components::H2O18<Scalar> H2O18; 
//                          typedef Dumux::Components::Air<Scalar> Air; 
//                          // atomic diffusion volumes
//                          // VH2O18 = sum(ni*Vi) = XX + 2.31 +6.11 = 25.14 (Tang et al., 2015)--> method, (Poling et al., 2001, p.11.11)--> values
//                          //Alternativ: V_D2O = 12.95 
//                                      //V_H2O = 13.1
//                                      //V_H2O_einzelwerten = V_H2 + V_O = 12,23
//                                      //V_H2O_ einzelwerten = 2*V_H + V_O  = 10.73
//                          // V_H2O18 wahrscheinlich leicht größer als V_H2O Annahme: V_H2O18 = 13.2
//                          V_Air = 19.7 --> value: Reid et al. (1987) 
//                          const Scalar SigmaNu[2] = {13.1 /*H2O18*/, 19.7 /*Air*/ }; 
//                          const Scalar M[2] = {H2O18::molarMass()*Scalar(1e3), Air::molarMass()*Scalar(1e3)}; 
//                          return fullerMethod(M, SigmaNu, temperature, pressure); 
//                 
                
                //ODER wie bei Braud (2005): 
                
                const Scalar Theta=1.88;
                const Scalar Daw=2.17e-5;  /* reference value */
                const Scalar pg0=1e5;     /* reference pressure */
                const Scalar T0=273.16;    /* reference temperature */
                Scalar Dgaw;
                
                using std::pow;
                Dgaw=Daw*(pg0/pressure)*pow((temperature/T0),Theta); //--> Vapor diffusivity of water in air 
                
                //nach Braud, 2005: Dgaw = DgH2O18 * bH2O18 
                const Scalar bH2O18=1.0285; // Merlivat (1978a)
                Scalar DgH2O18; 
                
                DgH2O18 = Dgaw/bH2O18; //Diffusionskoeffizient von H2O18 in Luft 
                
                
                return DgH2O18;
                
            }
            
            /*!
             * Lacking better data on water-air diffusion in liquids, we use at the
             * moment the diffusion coefficient of the air's main component nitrogen!!
             * \brief Diffusion coefficient \f$\mathrm{[m^2/s]}\f$ for molecular nitrogen in liquid water.
             *
             * \param temperature the temperature \f$\mathrm{[K]}\f$
             * \param pressure the phase pressure \f$\mathrm{[Pa]}\f$
             *
             * The empirical equations for estimating the diffusion coefficient in
             * infinite solution which are presented in Reid, 1987 all show a
             * linear dependency on temperature. We thus simply scale the
             * experimentally obtained diffusion coefficient of Ferrell and
             * Himmelblau by the temperature.
             *
             * See:
             * R. Reid et al. (1987, pp. 599) \cite reid1987 <BR>
             * R. Ferrell, D. Himmelblau (1967, pp. 111-115) \cite ferrell1967
             */
            template <class Scalar>
            static Scalar liquidDiffCoeff(Scalar temperature, Scalar pressure) // von Wasser-Luft 
            {
                        const Scalar Texp = 273.15 + 25; // [K]
                        const Scalar Dexp = 2.01e-9; // [m^2/s]
                        return Dexp * temperature/Texp;
                
                //DUNE_THROW(Dune::NotImplemented, "H2O18_AirBinarycoefficient::liquidDiffCoeff()"); 
            }
        };
        
    } // end namespace BinaryCoeff
} // end namespace Dumux

#endif
