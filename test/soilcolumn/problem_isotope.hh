// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPNCTests
 * \brief Problem where air is injected under a low permeable layer in a depth of 2700m.
 */
#ifndef DUMUX_TWOPNC_SOILCOLUMN_PROBLEM_HH
#define DUMUX_TWOPNC_SOILCOLUMN_PROBLEM_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/cctpfa.hh>
#include <dumux/porousmediumflow/2pnc/model.hh>
#include <dumux/porousmediumflow/problem.hh>
#include "H2OAirIsotopes.hh"
#include <dumux/io/readwritedatafile.hh>
#include <dumux/material/fluidmatrixinteractions/diffusivityconstanttortuosity.hh>

#include <dumux/io/gnuplotinterface.hh>

#include "spatialparams.hh"
#include "radiation.hh"
#include <cmath>
#include <iostream>
#include <fstream>

#include <dumux/volumevariables.hh>
#include <dumux/iofields.hh>

namespace Dumux {
    
    template <class TypeTag>
    class TwoPNCSoilColumnProblem;
    
    namespace Properties {
        // Create new type tags
        namespace TTag {
            struct TwoPNCSoilColumn { using InheritsFrom = std::tuple<TwoPNCNI>; };
            struct TwoPNCSoilColumnBox { using InheritsFrom = std::tuple<TwoPNCSoilColumn, CCTpfaModel>; };
        } // end namespace TTag
        
        // Set the grid type
        template<class TypeTag>
        struct Grid<TypeTag, TTag::TwoPNCSoilColumn> {using type = Dune::YaspGrid<1>; };
        
        // Set the problem property
        template<class TypeTag>
        struct Problem<TypeTag, TTag::TwoPNCSoilColumn> { using type = TwoPNCSoilColumnProblem<TypeTag>; };
        
        // Set fluid configuration
        template<class TypeTag>
        struct FluidSystem<TypeTag, TTag::TwoPNCSoilColumn>
        { using type = FluidSystems::H2OAirIsotopes<GetPropType<TypeTag, Properties::Scalar>>;};
        
        // Set the spatial parameters
        template<class TypeTag>
        struct SpatialParams<TypeTag, TTag::TwoPNCSoilColumn>
        {
            using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
            using Scalar = GetPropType<TypeTag, Properties::Scalar>;
            using type = TwoPNCSoilColumnSpatialParams<FVGridGeometry, Scalar>;
        };
        
        // Define whether mole(true) or mass (false) fractions are used
        template<class TypeTag>
        struct UseMoles<TypeTag, TTag::TwoPNCSoilColumn> { static constexpr bool value = true; };
        
        //! Set the default formulation to pw-Sn: This can be over written in the problem.
        template<class TypeTag>
        struct Formulation<TypeTag, TTag::TwoPNCSoilColumn> { static constexpr auto value = TwoPFormulation::p1s0; };
        
        //! Set the volume variables property
        template<class TypeTag>
        struct VolumeVariables<TypeTag, TTag::TwoPNCSoilColumn>
        {
        private:
            using PV = GetPropType<TypeTag, Properties::PrimaryVariables>;
            using FSY = GetPropType<TypeTag, Properties::FluidSystem>;
            using FST = GetPropType<TypeTag, Properties::FluidState>;
            using SSY = GetPropType<TypeTag, Properties::SolidSystem>;
            using SST = GetPropType<TypeTag, Properties::SolidState>;
            using MT = GetPropType<TypeTag, Properties::ModelTraits>;
            using PT = typename GetPropType<TypeTag, Properties::SpatialParams>::PermeabilityType;
            
            using Traits = TwoPNCVolumeVariablesTraits<PV, FSY, FST, SSY, SST, PT, MT>;
        public:
            using type = TwoPIsotopeVolumeVariables<Traits>;
        };
        
        template<class TypeTag>
        struct IOFields<TypeTag, TTag::TwoPNCSoilColumn> { using type = TwoPNCIsotopeIOFields; };
        //we set the initial and boundary mole fractions for the second == gas phase
        template<class TypeTag>
        struct SetMoleFractionsForFirstPhase<TypeTag, TTag::TwoPNCSoilColumn> { static constexpr bool value = false; };
        template<class TypeTag>
        struct EffectiveDiffusivityModel<TypeTag, TTag::TwoPNCSoilColumn> 
        {   
            using Scalar = GetPropType<TypeTag, Properties::Scalar>;
            using type = DiffusivityConstantTortuosity<Scalar>; 
            
        };
        
    } // end namespace Properties
    
    
    /*!
     * \ingroup TwoPNCTests
     */
    template <class TypeTag>
    class TwoPNCSoilColumnProblem : public PorousMediumFlowProblem<TypeTag>
    {
        using ParentType = PorousMediumFlowProblem<TypeTag>;
        using GridView = GetPropType<TypeTag, Properties::GridView>;
        using Scalar = GetPropType<TypeTag, Properties::Scalar>;
        using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
        
        enum {
            // Grid and world dimension
            dim = GridView::dimension,
            dimWorld = GridView::dimensionworld
        };
        
        using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
        using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
        using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
        using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
        using Element = typename GridView::template Codim<0>::Entity;
        using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
        using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
        using FVElementGeometry = typename FVGridGeometry::LocalView;
        using GridVolumeVariables = GetPropType<TypeTag, Properties::GridVolumeVariables>;
        using ElementVolumeVariables = typename GridVolumeVariables::LocalView;
        using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
        using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);
        using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
        
        //! Property that defines whether mole or mass fractions are used
        static constexpr bool useMoles = getPropValue<TypeTag, Properties::UseMoles>();
        using Radiation = typename Dumux::Radiation<Scalar, FVGridGeometry>;
        using H2O = Components::TabulatedComponent<Components::H2O<Scalar> >;
        using Air = Dumux::Components::Air<Scalar>;
        using HDO = Dumux::Components::HDO<Scalar>;
        using H2O18 = Dumux::Components::H2O18<Scalar>;
    public:
        TwoPNCSoilColumnProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
        : ParentType(fvGridGeometry)
        {
            // initialize the tables of the fluid system
            FluidSystem::init();
            
            name_ = getParam<std::string>("Problem.Name");
            
            
            temperatureInitial_ = 273.15 + 16.79375;
            
            
            // stating in the console whether mole or mass fractions are used
            if(useMoles)
            {
                std::cout<<"problem uses mole-fractions"<<std::endl;
            }
            else
            {
                std::cout<<"problem uses mass-fractions"<<std::endl;
            }
            
            temperatureDataFilePM_ =getParam<std::string>("Problem.TemperatureDataFile");
            readData(temperatureDataFilePM_, temperatureDataPM_);
            
            relativeHumidityDataFilePM_ =getParam<std::string>("Problem.RelativeHumidityDataFile");
            readData(relativeHumidityDataFilePM_, relativeHumidityDataPM_);
            
            
            // for laminar flow
            //boundaryLayerDataFilePM_ =getParam<std::string>("Problem.BoundaryLayerDataFile");
            //readData(boundaryLayerDataFilePM_, boundaryLayerDataPM_);
            
            soilcontentDataFilePM_ =getParam<std::string>("Problem.SoilcontentDataFile");
            readData(soilcontentDataFilePM_, soilcontentDataPM_);
            
            //for laminar/turbulent flow
            windSpeedDataFilePM_ =getParam<std::string>("Problem.WindSpeedDataFile");
            readData(windSpeedDataFilePM_, windSpeedDataPM_);
            
            radiationDataFilePM_ =getParam<std::string>("Problem.RadiationDataFile");
            readData(radiationDataFilePM_, radiationDataPM_);
            
            resistancefactorDataFilePM_ =getParam<std::string>("Problem.ResistancefactorDataFile");
            readData(resistancefactorDataFilePM_, resistancefactorDataPM_);
            
            atmosphericHDODataFilePM_ =getParam<std::string>("Problem.AtmosphericHDODataFile");
            readData(atmosphericHDODataFilePM_, atmosphericHDODataPM_);
            
            atmosphericH2O18DataFilePM_ =getParam<std::string>("Problem.AtmosphericH2O18DataFile");
            readData(atmosphericH2O18DataFilePM_, atmosphericH2O18DataPM_);
            
            soilTemperatureDataFilePM_ = getParam<std::string>("Problem.SoilTemperatureDataFile");
            readData(soilTemperatureDataFilePM_, soilTemperatureDataPM_);
            
            
            
            
            
        }
        
        void setTime(Scalar time)
        { time_ = time; }
        
        const Scalar time() const
        {return time_; }
        
        void setPreviousTime(Scalar time)
        { previousTime_ = time; }
        
        const Scalar previousTime() const
        {return previousTime_; }
        
        //! Called after every time step
        //! Output the total global exchange term
        void postTimeStep(const SolutionVector& curSol,
                          const GridVariables &gridVariables)
        {
            Scalar evaporation = 0.0;
            Scalar netRadiation = 0.0;
            
            
            
            if (!(time() < 0.0))
            {
                Scalar refTemperature = 273.15 + evaluateData(temperatureDataPM_, previousTime(), time());
                
                for (const auto& element : elements(this->fvGridGeometry().gridView()))
                {
                    auto fvGeometry = localView(this->fvGridGeometry());
                    fvGeometry.bindElement(element);
                    
                    auto elemVolVars = localView(gridVariables.curGridVolVars());
                    elemVolVars.bindElement(element, fvGeometry, curSol);
                    
                    for (auto&& scvf : scvfs(fvGeometry))
                        if (scvf.boundary())
                        {
                            evaporation += neumann(element, fvGeometry, elemVolVars, scvf)[Indices::conti0EqIdx]
                            * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor() * FluidSystem::molarMass(FluidSystem::H2OIdx);
                            evaporation += neumann(element, fvGeometry, elemVolVars, scvf)[Indices::conti0EqIdx+2]
                            * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor()* FluidSystem::molarMass(FluidSystem::HDOIdx);
                            evaporation += neumann(element, fvGeometry, elemVolVars, scvf)[Indices::conti0EqIdx+3]
                            * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor()* FluidSystem::molarMass(FluidSystem::H2O18Idx);
                            const auto radiation = Radiation::radationEquilibrium(element, elemVolVars[scvf.insideScvIdx()], scvf,  refTemperature, time());
                            netRadiation += radiation * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();
                            
                            
                        }
                }
            }
            // convert to kg/s if using mole fractions
            
            //evaporation = useMoles ? evaporation * FluidSystem::molarMass(FluidSystem::H2OIdx) : evaporation;
            //evaporation = useMoles ? evaporation * FluidSystem::molarMass(FluidSystem::HDOIdx) : evaporation;
            
            std::cout << "Soil evaporation rate: " << evaporation << " kg/s." << '\n';
            //do a gnuplot
            x_.push_back(time()/3600); // in seconds
            y_.push_back(evaporation);
            
            gnuplot_.resetPlot();
            gnuplot_.setXRange(0,std::max(time()/3600, 7200.0));
            gnuplot_.setYRange(- 5e-6, 4e-6);
            gnuplot_.setXlabel("time [h]");
            gnuplot_.setYlabel("kg/s");
            gnuplot_.addDataSetToPlot(x_, y_, "evaporation rate");
            gnuplot_.plot("evaporation rate");
            
            //do a gnuplot
            y2_.push_back(netRadiation);
            gnuplot2_.resetPlot();
            gnuplot2_.setXRange(0,std::max(time()/3600, 24.0));
            gnuplot2_.setYRange(-100, 1500);
            gnuplot2_.setXlabel("time [s]");
            gnuplot2_.setYlabel("W/m^2");
            gnuplot2_.addDataSetToPlot(x_, y2_, "net radiation");
            gnuplot2_.plot("radiation");
            
            
            
            
        }
        
        /*!
         * \name Problem parameters
         */
        // \{
        
        /*!
         * \brief Returns the problem name
         *
         * This is used as a prefix for files generated by the simulation.
         */
        
        const std::string& name() const
        { return name_; }
        
        /*!
         * \brief Returns the temperature [K]
         */
        Scalar temperature() const
        { return 293.15; }
        
        // \}
        
        /*!
         * \name Boundary conditions
         */
        // \{
        
        /*!
         * \brief Specifies which kind of boundary condition should be
         *        used for which equation on a given boundary segment
         *
         * \param globalPos The global position
         */
        BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
        {
            BoundaryTypes bcTypes;
            
            bcTypes.setAllNeumann();
            
            //           if(globalPos[0] < this->fvGridGeometry().bBoxMin()[0]+ eps_ )
            //           bcTypes.setAllDirichlet();
            
            return bcTypes;
        }
        
        
        
        
        /*!
         * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
         *
         * \param globalPos The global position
         *
         */
        
        //Primary variables for implemnting a Dirichlet boundary condition at the bottom of the soil column 
        //Based on the experiments a Neumann no-flow condtion is sufficient
        PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
        {
            PrimaryVariables priVars;
            priVars.setState(Indices::bothPhases);
            priVars[Indices::switchIdx] = 1.0; // soil column fully saurated at the bottom of the column 
            priVars[Indices::temperatureIdx] = temperatureInitial_;
            priVars[Indices::pressureIdx] = 1e5;
            Scalar R_D_L = delta_D_*ref_D_ /1000+ ref_D_; 
            Scalar R_O18_L = delta_O18_*ref_O18_/1000 + ref_O18_;
            Scalar R_D = R_D_L *(exp(-((24844/(temperatureInitial_*temperatureInitial_))+(-76.248/temperatureInitial_)+0.052612)));
            Scalar R_O18 = R_O18_L * (exp(-((1137/(temperatureInitial_*temperatureInitial_))+(-0.4156/temperatureInitial_)-0.0020667)));
            Scalar vaporPressure = H2O::vaporPressure(temperatureInitial_);
            priVars[Indices::conti0EqIdx+2] = R_D*vaporPressure/1e5;  
            priVars[Indices::conti0EqIdx+3] = R_O18*vaporPressure/1e5;
            
            return priVars;
        }
        
        /*!
         * \brief Evaluates the boundary conditions for a Neumann boundary segment.
         *
         * For this method, the \a priVars parameter stores the mass flux
         * in normal direction of each component. Negative values mean influx.
         * \param globalPos The global position
         *
         * \note The units must be according to either using mole or mass fractions (mole/(m^2*s) or kg/(m^2*s)).
         */
        
        NumEqVector neumann(const Element &element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolumeFace& scvf) const
                            {
                                NumEqVector values(0.0);
                                const auto globalPos = scvf.ipGlobal();
                                const auto& volVars = elemVolVars[scvf.insideScvIdx()];
                                
                                //Possibilities of determing the BoundaryLayerThickness and resistance factor 
                                
                                // BoundaryLayerThickness per InputFile
                                // Scalar boundaryLayerThickness = evaluateData(boundaryLayerDataPM_, previousTime(), time());
                                
                                //Boundary layer thickness via restistance factor 
                                //Resistance factor implemented per Input File; calculated by SiSPAT 
                                //Scalar rah = evaluateData(resistancefactorDataPM_, previousTime(), time());
                                //Scalar boundaryLayerThickness = rah * volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx); 
                                
                                //BoundaryLayerThickness denpending on laminar/turbulent flow
                                //Calculation of the resistance factor 
                                Scalar WindSpeed = evaluateData(windSpeedDataPM_, previousTime(), time());
                                double Re = WindSpeed * 1.8 * volVars.density(FluidSystem::gasPhaseIdx) / volVars.viscosity(FluidSystem::gasPhaseIdx); // 1.8 equals the measurement height of the wind speed
                                
                                Scalar boundaryLayerThickness = 0.0;
                                Scalar rah =0.0; 
                                
                                if (Re < 5*1e5) { //laminar flow (Schlichting, 2017; White, 2011)
                                    using std::sqrt;
                                    boundaryLayerThickness = 5.0*sqrt(volVars.viscosity(FluidSystem::gasPhaseIdx)*distance_ / (0.99*WindSpeed*volVars.density(FluidSystem::gasPhaseIdx)));
                                    rah = boundaryLayerThickness/volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx); 
                                }
                                
                                else { //turbulent flow (White, 2011)
                                    using std::pow;
                                    boundaryLayerThickness = 0.16 * distance_ / pow(Re, (1/7));
                                    rah = boundaryLayerThickness/volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx); 
                                    
                                }
                                
                                // Possiblities of determine the kinetic fractionation factor (a) Merlivat/Jouzel (b) Stewart 
                                using std::log;
                                Scalar FrictionVelocity = WindSpeed * 0.4 / log(height_/surfaceRoughness_);  
                                
                                Scalar kinematicVisocsity = volVars.viscosity(FluidSystem::gasPhaseIdx) /volVars.density(FluidSystem::gasPhaseIdx);
                                Scalar Rex = FrictionVelocity* surfaceRoughness_/ kinematicVisocsity;
                                double nK;
                                Scalar ratioResistance;
                                
                                
                                if (Rex < 1) { //laminar flow
                                    using std::log;
                                    using std::pow;
                                    nK = 0.666667;
                                    ratioResistance = 1.0/0.4 * log (FrictionVelocity*height_ / 30.0 / kinematicVisocsity) / (13.6*pow(kinematicVisocsity / volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx) ,nK));
                                }
                                
                                else {
                                    using std::pow;
                                    nK = 0.5;
                                    ratioResistance = (1.0/0.4*log (height_ / surfaceRoughness_) - 5.0) /  ( 7.3*pow(Rex, (1.0/4.0))*pow(kinematicVisocsity /volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx),nK));
                                }
                                
                                // 1. Approach based on Merlivat and Jouzel (1978)
                                Scalar alpha_k_HDO = (pow((volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx)/volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::HDOIdx)),nK) + ratioResistance) / ( 1.0+ratioResistance);
                                Scalar alpha_k_H2O18 = (pow((volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx)/volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::H2O18Idx)),nK) + ratioResistance) / ( 1.0+ratioResistance);
                                
                                
                                
                                //2. approach based on Stewart (1975)
                                //                             using std::pow;
                                //                             Scalar soilcontent = evaluateData(soilcontentDataPM_, previousTime(), time());
                                //                             Scalar nK = ((soilcontent - residualSaturation_)*0.5 +(saturatedWaterContent_- soilcontent))/ (saturatedWaterContent_ - residualSaturation_);
                                //                             
                                //                             Scalar alpha_k_HDO =pow((volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx)/volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::HDOIdx)),nK);
                                //                             Scalar alpha_k_H2O18 =pow((volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx)/volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::H2O18Idx)),nK);
                                
                                
                                
                                //Definiiton of the isotopic fractionatrion factor for equilbirum conditions (Majoube, 1971)
                                using std::exp;
                                using std::pow;
                                Scalar alphaHDO = exp(-((24844.0/(volVars.temperature()*volVars.temperature())+((-76.248)/volVars.temperature()) + 0.052612)));
                                Scalar alphaH2O18 = exp(-((1137.0/(volVars.temperature()*volVars.temperature())+((-0.4156)/volVars.temperature()) -0.0020667)));
                                
                                Scalar refTemperature = 273.15 + evaluateData(temperatureDataPM_, previousTime(), time());
                                
                                //Isotopic compositions in soil and in the atmosphere
                                //Mole Fraction in the atmosphere in vapour phase
                                Scalar refMoleFracVapor = evaluateData(relativeHumidityDataPM_, previousTime(), time())/100*H2O::vaporPressure(refTemperature)/1e5; //For H2O
                                Scalar refMoleFracHDOVapor = ((evaluateData(atmosphericHDODataPM_, previousTime(), time()))*ref_D_/1000+ref_D_)*alpha_k_HDO*refMoleFracVapor; //For HDO
                                Scalar refMoleFracH2O18Vapor = ((evaluateData(atmosphericH2O18DataPM_, previousTime(), time()))*ref_O18_/1000+ref_O18_)*alpha_k_H2O18*refMoleFracVapor; //For H2O18
                                // std::cout<<"ref mole frac h18o "<<refMoleFracH2O18Vapor<<std::endl;
                                Scalar refMoleFracAirVapor = 1.0 - refMoleFracVapor-refMoleFracH2O18Vapor-refMoleFracHDOVapor; //For Air 
                                // std::cout<<"ref mole frac hdo "<<refMoleFracHDOVapor<<std::endl;
                                //Average Molar Mass in Atmosphere in vapour phase
                                Scalar refaverageMolarMass = FluidSystem::molarMass(FluidSystem::H2OIdx)*refMoleFracVapor + FluidSystem::molarMass(FluidSystem::AirIdx)*refMoleFracAirVapor +FluidSystem::molarMass(FluidSystem::HDOIdx)*refMoleFracHDOVapor + FluidSystem::molarMass(FluidSystem::H2O18Idx)*refMoleFracH2O18Vapor;
                                
                                //Mass Fraction in the atmosphere in vapour phase
                                Scalar refMassFracVapor = refMoleFracVapor* FluidSystem::molarMass(FluidSystem::H2OIdx)/refaverageMolarMass; // For H2O 
                                Scalar refMassFracHDOVapor= refMoleFracHDOVapor*FluidSystem::molarMass(FluidSystem::HDOIdx)/refaverageMolarMass; //For HDO
                                Scalar refMassFracH2O18Vapor = refMoleFracH2O18Vapor*FluidSystem::molarMass(FluidSystem::H2O18Idx)/refaverageMolarMass; //For H2O18
                                
                                //Mass fractions in soil in vapour phase
                                Scalar massFracH2OInside = volVars.massFraction(FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx); //For H2O 
                                Scalar massFracHDOInside = volVars.massFraction(FluidSystem::gasPhaseIdx, FluidSystem::HDOIdx); //For HDO
                                Scalar massFracH2O18Inside = volVars.massFraction(FluidSystem::gasPhaseIdx, FluidSystem::H2O18Idx); //For H2O18
                                
                                //Mole fractions in soil in liquid phase
                                //                             Scalar moleFracH2OInside = volVars.moleFraction(FluidSystem::liquidPhaseIdx, FluidSystem::H2OIdx); //For H2O 
                                //                             Scalar moleFracHDOInside = volVars.moleFraction(FluidSystem::liquidPhaseIdx, FluidSystem::HDOIdx); //For HDO
                                //                             Scalar moleFracH2O18Inside = volVars.moleFraction(FluidSystem::liquidPhaseIdx, FluidSystem::H2O18Idx); //For H2O18
                                
                                //Mass fractions in soil in liquid phase
                                Scalar massFracH2OInsideliquid = volVars.massFraction(FluidSystem::liquidPhaseIdx, FluidSystem::H2OIdx); //For H2O 
                                Scalar massFracHDOInsideliquid = volVars.massFraction(FluidSystem::liquidPhaseIdx, FluidSystem::HDOIdx); //For HDO
                                Scalar massFracH2O18Insideliquid = volVars.massFraction(FluidSystem::liquidPhaseIdx, FluidSystem::H2O18Idx); //For H2O18
                                
                                
                                //normalized humidity 
                                Scalar soilTemperature = evaluateData(soilTemperatureDataPM_, previousTime(), time()); //measured soil temperature at the soil-atmosphere-interface
                                Scalar humidity =  evaluateData(relativeHumidityDataPM_, previousTime(), time())/100 * H2O::vaporPressure(refTemperature)/H2O::vaporPressure(soilTemperature);           
                                
                                if(globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_) // upper Neumann boundary condition 
                                {
                                    
                                    //Calculation of evaporation flux 
                                    Scalar evaporationHDORateMole = volVars.density(FluidSystem::gasPhaseIdx)*(massFracHDOInsideliquid*alphaHDO*massFracH2OInside - refMassFracHDOVapor)/alpha_k_HDO/rah/FluidSystem::molarMass(FluidSystem::H2OIdx); //For HDO Flux
                                    Scalar evaporationH2O18RateMole = volVars.density(FluidSystem::gasPhaseIdx)*(massFracH2O18Insideliquid*alphaH2O18*massFracH2OInside - refMassFracH2O18Vapor)/alpha_k_H2O18/rah/FluidSystem::molarMass(FluidSystem::H2OIdx); //For H2O18 Flux 
                                    Scalar evaporationRateMole = (massFracH2OInside - refMassFracVapor-refMassFracH2O18Vapor-refMassFracHDOVapor)/ rah /FluidSystem::molarMass(FluidSystem::H2OIdx) * volVars.density(FluidSystem::gasPhaseIdx); //For H2O Flux 
                                    
                                    
                                    values[Indices::conti0EqIdx] = evaporationRateMole;
                                    values[Indices::conti0EqIdx+1] = -evaporationRateMole;
                                    values[Indices::conti0EqIdx+2] = evaporationHDORateMole;
                                    values[Indices::conti0EqIdx+1] += -evaporationHDORateMole;
                                    values[Indices::conti0EqIdx+3] = evaporationH2O18RateMole;
                                    values[Indices::conti0EqIdx+1] += -evaporationH2O18RateMole;
                                    
                                    
                                    
                                    if (volVars.pressure(FluidSystem::gasPhaseIdx) - 1e5 > 0) {
                                        values[Indices::conti0EqIdx+1] = (volVars.pressure(FluidSystem::gasPhaseIdx) - 1e5)
                                        /(globalPos - fvGeometry.scv(scvf.insideScvIdx()).center()).two_norm()
                                        *volVars.mobility(FluidSystem::gasPhaseIdx)
                                        *this->spatialParams().permeabilityAtPos(globalPos)
                                        *volVars.molarDensity(FluidSystem::gasPhaseIdx) * volVars.moleFraction(FluidSystem::gasPhaseIdx, FluidSystem::AirIdx);
                                    }
                                    else {
                                        values[Indices::conti0EqIdx+1] = (volVars.pressure(FluidSystem::gasPhaseIdx) - 1e5)
                                        /(globalPos - fvGeometry.scv(scvf.insideScvIdx()).center()).two_norm()
                                        *volVars.mobility(FluidSystem::gasPhaseIdx)
                                        *this->spatialParams().permeabilityAtPos(globalPos)
                                        *volVars.molarDensity(FluidSystem::gasPhaseIdx) * (1-refMoleFracVapor-refMoleFracHDOVapor-refMoleFracH2O18Vapor);
                                    }
                                    
                                    values[Indices::energyEqIdx] = H2O::gasEnthalpy(volVars.temperature(),volVars.pressure(FluidSystem::gasPhaseIdx)) * values[Indices::conti0EqIdx]*FluidSystem::molarMass(FluidSystem::H2OIdx);            
                                    
                                    values[Indices::energyEqIdx] += HDO::gasEnthalpy(volVars.temperature(),volVars.pressure(FluidSystem::gasPhaseIdx)) * values[Indices::conti0EqIdx+2]*FluidSystem::molarMass(FluidSystem::HDOIdx);
                                    
                                    values[Indices::energyEqIdx] += H2O18::gasEnthalpy(volVars.temperature(),volVars.pressure(FluidSystem::gasPhaseIdx)) * values[Indices::conti0EqIdx+3]*FluidSystem::molarMass(FluidSystem::H2O18Idx);
                                    
                                    values[Indices::energyEqIdx] += Air::gasEnthalpy(refTemperature,1e5) * values[Indices::conti0EqIdx+1]*FluidSystem::molarMass(FluidSystem::AirIdx);
                                    
                                    values[Indices::energyEqIdx] += Air::gasThermalConductivity(refTemperature, 1)* (volVars.temperature() - refTemperature)/boundaryLayerThickness;
                                    
                                    //get radiation from input data
                                    values[Indices::energyEqIdx] += -1*evaluateData(radiationDataPM_, previousTime(), time());
                                    
                                    //get calculated radiation
                                    // values[Indices::energyEqIdx] += -1*Radiation::radationEquilibrium(element, elemVolVars[scvf.insideScvIdx()], scvf,  refTemperature, time());
                                }
                                
                                return values;
                            }
                            
                            
                            // \}
                            
                            /*!
                             * \name Volume terms
                             */
                            // \{
                            
                            /*!
                             * \brief Evaluates the initial values for a control volume.
                             *
                             * \param globalPos The global position
                             */
                            PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
                            {
                                return initial_(globalPos);
                            }
                            
                            // \}
                            
    private:
        /*!
         * \brief Evaluates the initial values for a control volume.
         *
         * The internal method for the initial condition
         *
         * \param globalPos The global position
         */
        PrimaryVariables initial_(const GlobalPosition &globalPos) const
        {
            PrimaryVariables priVars(0.0);
            priVars.setState(Indices::bothPhases);
            
            
            
            Scalar R_D_L = delta_D_*ref_D_/1000+ref_D_;
            Scalar R_O18_L = delta_O18_*ref_O18_/1000+ref_O18_;
            Scalar R_D = R_D_L *(exp(-((24844/(temperatureInitial_*temperatureInitial_))+(-76.248/temperatureInitial_)+0.052612)));
            Scalar R_O18 = R_O18_L *(exp(-((1137/(temperatureInitial_*temperatureInitial_))+(-0.4156/temperatureInitial_)-0.0020667))); 
            
            Scalar vaporPressure = H2O::vaporPressure(temperatureInitial_);
            Scalar gasPressure = 1e5;
            //Scalar gasPressure = 1e5 + (this->fvGridGeometry().bBoxMax()[0] - globalPos[0])*Air::gasDensity(temperatureInitial_,1e5)*9.81;
            
            
            priVars[Indices::conti0EqIdx+2] = R_D*vaporPressure/1e5;
            priVars[Indices::conti0EqIdx+3] = R_O18*vaporPressure/1e5;

            
            //priVars[Indices::conti0EqIdx+2] = R_D;
            // priVars[Indices::conti0EqIdx+3] = R_O18;
            
            //         //mole-fraction formulation
            //         if (globalPos[0] < 1.0 + eps_)
            //             priVars[Indices::switchIdx] =  0.92;
            //         //For Sucre: 0.9
            //         //For Pohlmeier A: 0.81
            //         //For Pohlmeier B: 0.81
            //         //For Stingaciu: 0.81
            //         //For Snehota: 0.9
            //         //For Haber-Pohlmeier: 0.82
            //         
            //                     else if (globalPos[0] > 1.0 + eps_ && globalPos[0] < 1.2 - eps_)
            //                         priVars[Indices::switchIdx] =  0.89;
            //                     else if (globalPos[0] > 1.2 + eps_ && globalPos[0] < 1.45 - eps_)
            //                         priVars[Indices::switchIdx] =  0.88;
            //         else
            priVars[Indices::switchIdx] =  0.9;
            
            //For Sucre: 0.79
            //For Pohlmeier A: 0.71
            //For Pohlmeier B: 0.71
            //For Stingaciu: 0.71
            //For Snehota: 0.79
            //For Haber-Pohlmeier: 0.72
            
            priVars[Indices::pressureIdx] = gasPressure;
            
            //         if (globalPos[0] < 1.0 + eps_)
            //             priVars[Indices::temperatureIdx] = 273.15 + 18.4333333333333;
            //                     else if (globalPos[0] > 1.0 + eps_ && globalPos[0] < 1.2 - eps_)
            //                         priVars[Indices::temperatureIdx] = 273.15 + 19.9133333333333;
            //                     else if (globalPos[0] > 1.2 + eps_ && globalPos[0] < 1.45 - eps_)
            //                         priVars[Indices::temperatureIdx] = 273.15 + 20.1466666666667;
            //         else
            priVars[Indices::temperatureIdx] = 273.15 + 16.79375;
            return priVars;
        }
        
        Scalar temperature_;
        static constexpr Scalar eps_ = 1e-6;
        std::string name_ ;
        Scalar temperatureInitial_;
        
        Scalar time_;
        Scalar previousTime_;
        Scalar distance_ =  0.00075;// [m]  distance downstream from start of the boundary layer; free varaiable 
        Scalar height_ = 1.8; // [m] Measurement height of wind speed
        Scalar surfaceRoughness_= 0.0005; // [m]  mean particle size of sand FH31
        
        //soil specific values based on Sucre(2011)
        Scalar porosity_ = 0.36226415; // Porosität [-]
        Scalar residualSaturation_ = 0.035;  // residual water content [cm3/cm3]
        Scalar saturatedWaterContent_ = 0.34; // saturated water content 
        
        //reference values of the delta notation 
        Scalar ref_D_ = 155.76e-6;
        Scalar ref_O18_ = 2005.2e-6; 
        //inital isotopic composition
        Scalar delta_D_ = - 53.5; 
        Scalar delta_O18_ = - 8.2;  
        
        
        std::string temperatureDataFilePM_;
        std::vector<double> temperatureDataPM_[2];
        std::string relativeHumidityDataFilePM_;
        std::vector<double> relativeHumidityDataPM_[2];
        
        
        std::string soilcontentDataFilePM_;
        std::vector<double> soilcontentDataPM_[2];
        
        std::string resistancefactorDataFilePM_;
        std::vector<double> resistancefactorDataPM_[2];
        
        // for laminar flow
        //std::string boundaryLayerDataFilePM_;
        //std::vector<double> boundaryLayerDataPM_[2];
        
        // for laminar/turbulent flow
        std::string windSpeedDataFilePM_;
        std::vector<double> windSpeedDataPM_[2];
        
        std::string radiationDataFilePM_;
        std::vector<double> radiationDataPM_[2];
        
        
        
        std::string atmosphericHDODataFilePM_;
        std::vector<double> atmosphericHDODataPM_[2];
        
        std::string atmosphericH2O18DataFilePM_;
        std::vector<double> atmosphericH2O18DataPM_[2];
        
        std::string soilTemperatureDataFilePM_;
        std::vector<double> soilTemperatureDataPM_[2];
        
        
        Dumux::GnuplotInterface<double> gnuplot_;
        Dumux::GnuplotInterface<double> gnuplot2_;
        
        std::vector<double> x_;
        std::vector<double> y_;
        std::vector<double> y2_;
        
        
        
    };
    
} // end namespace Dumux

#endif
