// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Class for calculating radiation at the interface of a pm - ff model
 */

#ifndef DUMUX_RADIATION_EQUILIBRIUM_HH
#define DUMUX_RADIATION_EQUILIBRIUM_HH

namespace Dumux {

/*!
 * \brief Class for calculating radiation at the interface of a pm - ff model
 */
template <class Scalar, class FVGridGeometry>
class Radiation
{
    using GridView = typename FVGridGeometry::GridView;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using Element = typename GridView::template Codim<0>::Entity;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

public:
    template<class VolumeVariables>
    static Scalar radationEquilibrium(const Element& element,
                                      const VolumeVariables &volVars,
                                      const SubControlVolumeFace& scvf,
                                      const Scalar ffTemp,
                                      const Scalar time)
    {
        using FluidSystem = typename VolumeVariables::FluidSystem;

        Scalar solarIrradiance = 0.0;
        Scalar surfaceAlbedo = 0.0;
        //stefan-boltzmannConstant
        Scalar boltzmannConstant = 5.670367e-8; //W/m^2K^4
        Scalar surfaceEmissivity = 0.0;
        Scalar atmosphericEmissivity = 0.0;
        Scalar surfaceTemp = 0.0;
        Scalar waterContent = volVars.saturation(FluidSystem::liquidPhaseIdx)*volVars.porosity();
        Scalar timeInHours = time/3600.0; //time in hours

        surfaceTemp +=  volVars.temperature();

        using std::pow;
        //the vaporPressure is in Pa but has to be in hPa therefore we devide 100
        Scalar exponent = 1.0/7.0;
        //TODO: 
        atmosphericEmissivity += 1.24*pow((FluidSystem::H2O::vaporPressure(ffTemp)/100/ffTemp),exponent);
       

        if (waterContent < 0.3)
            surfaceEmissivity += 0.93 + 0.1333*waterContent;
        else
            surfaceEmissivity += 0.97;

        if (waterContent > 0.3)
            surfaceAlbedo += 0.075;
        else if (waterContent < 0.04)
            surfaceAlbedo += 0.17;
        else
            surfaceAlbedo += 0.1846 - 0.3654*waterContent;

        using std::cos;
        Scalar s = 800*cos(2*M_PI*((timeInHours + 12)/24));
        //if solarIrradiance is smaller than 0 it means it is night
        if (s < 0.0)
            solarIrradiance = 0.0;
        else
            solarIrradiance += s;

        Scalar radiation = solarIrradiance*(1-surfaceAlbedo)+(boltzmannConstant*surfaceEmissivity*(atmosphericEmissivity*pow(ffTemp,4.0) - pow(surfaceTemp, 4.0)));

        return radiation;
    }


};

} // end namespace Dumux

#endif
