// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPNCTests
 * \brief Definition of the spatial parameters for the fuel cell
 *        problem which uses the isothermal/non-insothermal 2pnc box model.
 */

#ifndef DUMUX_TWOPNC_DIFFUSION_SPATIAL_PARAMS_HH
#define DUMUX_TWOPNC_DIFFUSION_SPATIAL_PARAMS_HH

#include <dumux/porousmediumflow/properties.hh>
#include <dumux/material/spatialparams/fv.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>

namespace Dumux {
/*!
 * \ingroup TwoPNCTests
 * \brief Definition of the spatial parameters for the TwoPNCSoilColumn
 *        problem which uses the isothermal 2p2c box model.
 */
template<class FVGridGeometry, class Scalar>
class TwoPNCSoilColumnSpatialParams
: public FVSpatialParams<FVGridGeometry, Scalar,
                         TwoPNCSoilColumnSpatialParams<FVGridGeometry, Scalar>>
{
    using GridView = typename FVGridGeometry::GridView;
    using ParentType = FVSpatialParams<FVGridGeometry, Scalar,
                                       TwoPNCSoilColumnSpatialParams<FVGridGeometry, Scalar>>;

    static constexpr int dimWorld = GridView::dimensionworld;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using DimWorldMatrix = Dune::FieldMatrix<Scalar, dimWorld, dimWorld>;

    using EffectiveLaw = RegularizedVanGenuchten<Scalar>;

public:
    using PermeabilityType = Scalar;

    using MaterialLaw = EffToAbsLaw<EffectiveLaw>;
    using MaterialLawParams = typename MaterialLaw::Params;

    TwoPNCSoilColumnSpatialParams(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    
    {
        //Sb10
//         materialParams1_.setSwr(0.181);
//         materialParams1_.setSnr(0.0081);
//         // parameters for the vanGenuchten law
//         materialParams1_.setVgAlpha(0.000074); //first horizon
//         materialParams1_.setVgn(1.5);
// 
//         // residual saturations
//         materialParams2_.setSwr(0.141);
//         materialParams2_.setSnr(0.0734);
//         // parameters for the vanGenuchten law
//         materialParams2_.setVgAlpha(0.000065); //first horizon
//         materialParams2_.setVgn(1.5901);  
//         
//                 // residual saturations
//         materialParams3_.setSwr(0.141);
//         materialParams3_.setSnr(0.0813);
//         // parameters for the vanGenuchten law
//         materialParams3_.setVgAlpha(0.000055); //first horizon
//         materialParams3_.setVgn(1.6528);    
//         
//                 // residual saturations
//         materialParams4_.setSwr(0.156);
//         materialParams4_.setSnr(0.0588);
//         // parameters for the vanGenuchten law
//         materialParams4_.setVgAlpha(0.000056); //first horizon
//         materialParams4_.setVgn(1.6385);    
//         
        
        
        // residual saturations
        //For Sucre (2011)
        
        materialParams1_.setSwr(0.097);
        materialParams1_.setSnr(0.0615);
        // parameters for the vanGenuchten law
        materialParams1_.setVgAlpha(0.000385); //first horizon
        materialParams1_.setVgn(8.5);
// // 
//         // residual saturations
//         materialParams2_.setSwr(0.097);
//         materialParams2_.setSnr(0.0615);
//         // parameters for the vanGenuchten law
//         materialParams2_.setVgAlpha(0.000385); //first horizon
//         materialParams2_.setVgn(8.5);
//         
        // residual saturations
        //For Pohlmeier A (2009)
        
//         materialParams1_.setSwr(0.05);
//         materialParams1_.setSnr(0.1827);
//         // parameters for the vanGenuchten law
//         materialParams1_.setVgAlpha(0.000134); //first horizon
//         materialParams1_.setVgn(12.72);
// 
//         // residual saturations
//         materialParams2_.setSwr(0.050);
//         materialParams2_.setSnr(0.1827);
//         // parameters for the vanGenuchten law
//         materialParams2_.setVgAlpha(0.000134); //first horizon
//         materialParams2_.setVgn(12.72);
        
        
        // residual saturations
        //For Pohlmeier B (2009)
        
//         materialParams1_.setSwr(0.05);
//         materialParams1_.setSnr(0.049);
//         // parameters for the vanGenuchten law
//         materialParams1_.setVgAlpha(0.000134); //first horizon
//         materialParams1_.setVgn(12.72);
// 
//         // residual saturations
//         materialParams2_.setSwr(0.05);
//         materialParams2_.setSnr(0.049);
//         // parameters for the vanGenuchten law
//         materialParams2_.setVgAlpha(0.000134); //first horizon
//         materialParams2_.setVgn(12.72);
//         
        // residual saturations
        //For Stingaciu (2009)
/*        
        materialParams1_.setSwr(0.05);
        materialParams1_.setSnr(0.2050);
        // parameters for the vanGenuchten law
        materialParams1_.setVgAlpha(0.00033); //first horizon
        materialParams1_.setVgn(5.4);

        materialParams2_.setSwr(0.05);
        materialParams2_.setSnr(0.2050);
        // parameters for the vanGenuchten law
        materialParams2_.setVgAlpha(0.00033); //first horizon
        materialParams2_.setVgn(5.4);*/
        
        
        // residual saturations
        //For Snehota (2014)
      
//         materialParams1_.setSwr(0.077);
//        // materialParams1_.setSnr(-0.1273);
//         materialParams1_.setSnr(0.025);
//        // parameters for the vanGenuchten law
//         materialParams1_.setVgAlpha(0.000370); //first horizon
//         materialParams1_.setVgn(5.9133);
// 
//        // residual saturations
//         materialParams2_.setSwr(0.077);
//         //materialParams2_.setSnr(-0.1273);
//         materialParams2_.setSnr(0.025);
//        // parameters for the vanGenuchten law
//         materialParams2_.setVgAlpha(0.000370); //first horizon
//         materialParams2_.setVgn(5.9133);
        
        
        // residual saturations
        //For Haber-Pohlmeier (2010)
        
//         materialParams1_.setSwr(0.05);
//         materialParams1_.setSnr(0.025);
//         // parameters for the vanGenuchten law
//         materialParams1_.setVgAlpha(0.00013); //first horizon
//         materialParams1_.setVgn(12.7);
// 
//         // residual saturations
//         materialParams2_.setSwr(0.05);
//         materialParams2_.setSnr(0.025);
//         // parameters for the vanGenuchten law
//         materialParams2_.setVgAlpha(0.00013); //first horizon
//         materialParams2_.setVgn(12.7);
//         
        
        

//         // residual saturations
//         materialParams3_.setSwr(0.141);
//         materialParams3_.setSnr(0.0813);
//         // parameters for the vanGenuchten law
//         materialParams3_.setVgAlpha(0.000055); //first horizon
//         materialParams3_.setVgn(1.6528);
//
//         // residual saturations
//         materialParams4_.setSwr(0.156);
//         materialParams4_.setSnr(0.0588);
//         // parameters for the vanGenuchten law
//         materialParams4_.setVgAlpha(0.000056); //first horizon
//         materialParams4_.setVgn(1.6385);
//
//         materialParams5_.setSwr(0.107);
//         materialParams5_.setSnr(0.0693);
//         materialParams5_.setVgAlpha(0.000391);
//         materialParams5_.setVgn(1.3064);
        
        
        // residual saturations
        //Jana
        
//         materialParams1_.setSwr(0.106);
//         materialParams1_.setSnr(0.01);
//         // parameters for the vanGenuchten law
//         materialParams1_.setVgAlpha(0.000385); //first horizon
//         materialParams1_.setVgn(8.5);
// 
//         // residual saturations
//         materialParams2_.setSwr(0.106);
//         materialParams2_.setSnr(0.01);
//         // parameters for the vanGenuchten law
//         materialParams2_.setVgAlpha(0.000385); //first horizon
//         materialParams2_.setVgn(8.5);
        
    }

    /*!
     * \brief Returns the hydraulic conductivity \f$[m^2]\f$
     *
     * \param globalPos The global position
     */
    PermeabilityType porosityAtPos(const GlobalPosition& globalPos) const
    {
//        if (globalPos[0] < 0.38 - eps_)
//            return 0.465257861635219;
//         else if (globalPos[0] > 0.38 + eps_ && globalPos[0] < .67 - eps_)
//             return 0.443974842767294;
//         else if (globalPos[0] > 0.67 + eps_ && globalPos[0] < 1.24 - eps_)
//             return 0.397597484276728;
//     //    else if (globalPos[0] > 0.75 + eps_ && globalPos[0] < 1.21 - eps_)
//       //      return 0.437735849056604;
//        else
//            return 0.376666666666668; 
//             //for Sucre
           return 0.362264150943396;
            //for Pohlmeier A
//           return 0.40377358490566;        
//             //for Pohlmeier B
//             return 0.40377358490566;        
//             //for Stingaciu
/*           return 0.40377358490566;  */      
//             //for Snehota
//            return 0.361940298507463;
//             //for Haber-Pohlmeier
//            return 0.4;
            //for Jana
//            return 0.33;
    }

    /*!
     * \brief Defines the porosity \f$[-]\f$ of the spatial parameters
     *
     * \param globalPos The global position
     */
    Scalar permeabilityAtPos(const GlobalPosition& globalPos) const
    {
//         if (globalPos[0] < 0.38 - eps_)
//             return 4.6265905093669e-13;
//         else if (globalPos[0] > .38 + eps_ && globalPos[0] < 0.67 - eps_)
//             return 5.23282350107241e-12;
//         else if (globalPos[0] > 0.67 + eps_ && globalPos[0] < 1.24 - eps_)
//             return 7.73158364154224e-14;
//         else if (globalPos[0] > 0.75 + eps_ && globalPos[0] < 1.21 - eps_)
//             return 6.16087962962963E-12;
//         else
//         return 1.68991109916379e-12; 
//             //for Sucre
           return 1.50462963E-12;
//             //for Pohlmeier A
//             return 1.30000000E-12;        
//             //for Pohlmeier B
//             return 1.40000000E-12;        
//             //for Stingaciu
//             return 1.29527778E-12;        
//             //for Snehota
//             return 1.29000000E-12;
//             //for Haber-Pohlmeier
//             return 4.52500000E-12;
           //for Jana
//            return 1.3E-12;
    }
    


    /*!
     * \brief Returns the parameter object for the Brooks-Corey material law
     * which depends on the position.
     *
     * \param globalPos The global position
     */
    const MaterialLawParams& materialLawParamsAtPos(const GlobalPosition& globalPos) const
    {
//         if (globalPos[0] < 1.0 - eps_)
//             return materialParams4_;
//         else if (globalPos[0] > 0.38 + eps_ && globalPos[0] < 0.67 - eps_)
//             return materialParams3_;
//         else if (globalPos[0] > 0.67 + eps_ && globalPos[0] < 1.24 - eps_)
//             return materialParams2_;
//         else if (globalPos[0] > 0.75 + eps_ && globalPos[0] < 1.21 - eps_)
//             return materialParams2_;
//         else
            return materialParams1_;
    }

    /*!
     * \brief Function for defining which phase is to be considered as the wetting phase.
     *
     * \param globalPos The position of the center of the element
     * \return The wetting phase index
     */
    template<class FluidSystem>
    int wettingPhaseAtPos(const GlobalPosition& globalPos) const
    { return FluidSystem::H2OIdx; }

private:
    static constexpr Scalar eps_ = 1e-6;
    MaterialLawParams materialParams1_;
//     MaterialLawParams materialParams2_;
//     MaterialLawParams materialParams3_;
//     MaterialLawParams materialParams4_;
//     MaterialLawParams materialParams5_;
};

} // end namespace Dumux

#endif
