// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPNCTests
 * \brief Problem where air is injected under a low permeable layer in a depth of 2700m.
 */
#ifndef DUMUX_TWOPNC_DIFFUSION_PROBLEM_HH
#define DUMUX_TWOPNC_DIFFUSION_PROBLEM_HH

#include <dune/grid/yaspgrid.hh>

#include <dumux/discretization/cctpfa.hh>
#include <dumux/porousmediumflow/2pnc/model.hh>
#include <dumux/porousmediumflow/problem.hh>
#include <dumux/material/fluidsystems/h2on2.hh>
#include <dumux/io/readwritedatafile.hh>

#include <dumux/io/gnuplotinterface.hh>

#include "spatialparams.hh"
#include "radiation.hh"
#include <cmath>
#include <iostream>
#include <fstream>

#include <dumux/volumevariables.hh>


namespace Dumux {

    template <class TypeTag>
    class TwoPNCSoilColumnProblem;

    namespace Properties {
        // Create new type tags
        namespace TTag {
            struct TwoPNCSoilColumn { using InheritsFrom = std::tuple<TwoPNCNI>; };
            struct TwoPNCSoilColumnBox { using InheritsFrom = std::tuple<TwoPNCSoilColumn, CCTpfaModel>; };
        } // end namespace TTag

        // Set the grid type
        template<class TypeTag>
        struct Grid<TypeTag, TTag::TwoPNCSoilColumn> { using type = Dune::YaspGrid<1>; };

        // Set the problem property
        template<class TypeTag>
        struct Problem<TypeTag, TTag::TwoPNCSoilColumn> { using type = TwoPNCSoilColumnProblem<TypeTag>; };

        // // Set fluid configuration
        template<class TypeTag>
        struct FluidSystem<TypeTag, TTag::TwoPNCSoilColumn>
        {
            using type = FluidSystems::H2ON2<GetPropType<TypeTag, Properties::Scalar>,
            FluidSystems::H2ON2DefaultPolicy</*fastButSimplifiedRelations=*/true>>;
        };

        // Set the spatial parameters
        template<class TypeTag>
        struct SpatialParams<TypeTag, TTag::TwoPNCSoilColumn>
        {
            using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
            using Scalar = GetPropType<TypeTag, Properties::Scalar>;
            using type = TwoPNCSoilColumnSpatialParams<FVGridGeometry, Scalar>;
        };

        // Define whether mole(true) or mass (false) fractions are used
        template<class TypeTag>
        struct UseMoles<TypeTag, TTag::TwoPNCSoilColumn> { static constexpr bool value = true; };

        //! Set the default formulation to pw-Sn: This can be over written in the problem.
        template<class TypeTag>
        struct Formulation<TypeTag, TTag::TwoPNCSoilColumn>
        { static constexpr auto value = TwoPFormulation::p1s0; };


        //! Set the volume variables property
        template<class TypeTag>
        struct VolumeVariables<TypeTag, TTag::TwoPNCSoilColumn>
        {
        private:
            using PV = GetPropType<TypeTag, Properties::PrimaryVariables>;
            using FSY = GetPropType<TypeTag, Properties::FluidSystem>;
            using FST = GetPropType<TypeTag, Properties::FluidState>;
            using SSY = GetPropType<TypeTag, Properties::SolidSystem>;
            using SST = GetPropType<TypeTag, Properties::SolidState>;
            using MT = GetPropType<TypeTag, Properties::ModelTraits>;
            using PT = typename GetPropType<TypeTag, Properties::SpatialParams>::PermeabilityType;

            using Traits = TwoPNCVolumeVariablesTraits<PV, FSY, FST, SSY, SST, PT, MT>;
        public:
            using type = TwoPIsotopeVolumeVariables<Traits>;
        };

    } // end namespace Properties


    /*!
     * \ingroup TwoPNCTests
     */
    template <class TypeTag>
    class TwoPNCSoilColumnProblem : public PorousMediumFlowProblem<TypeTag>
    {
        using ParentType = PorousMediumFlowProblem<TypeTag>;
        using GridView = GetPropType<TypeTag, Properties::GridView>;
        using Scalar = GetPropType<TypeTag, Properties::Scalar>;
        using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;

        enum {
            // Grid and world dimension
            dim = GridView::dimension,
            dimWorld = GridView::dimensionworld
        };

        using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
        using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
        using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
        using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
        using Element = typename GridView::template Codim<0>::Entity;
        using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
        using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
        using FVElementGeometry = typename FVGridGeometry::LocalView;
        using GridVolumeVariables = GetPropType<TypeTag, Properties::GridVolumeVariables>;
        using ElementVolumeVariables = typename GridVolumeVariables::LocalView;
        using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
        using SolutionVector = typename GET_PROP_TYPE(TypeTag, SolutionVector);
        using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;

        //! Property that defines whether mole or mass fractions are used
        static constexpr bool useMoles = getPropValue<TypeTag, Properties::UseMoles>();
        using Radiation = typename Dumux::Radiation<Scalar, FVGridGeometry>;
        using H2O = Components::TabulatedComponent<Components::H2O<Scalar> >;
        using Air = Dumux::Components::N2<Scalar>;

    public:
        TwoPNCSoilColumnProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
        : ParentType(fvGridGeometry)
        {
            // initialize the tables of the fluid system
            FluidSystem::init();

            name_ = getParam<std::string>("Problem.Name");

            //TODO: Change temperature for different soil (am untersten Messpunkt?)
            temperatureInitial_ = 273.15 + 18.4;

            // stating in the console whether mole or mass fractions are used
            if(useMoles)
            {
                std::cout<<"problem uses mole-fractions"<<std::endl;
            }
            else
            {
                std::cout<<"problem uses mass-fractions"<<std::endl;
            }

            temperatureDataFilePM_ =getParam<std::string>("Problem.TemperatureDataFile");
            readData(temperatureDataFilePM_, temperatureDataPM_);

            relativeHumidityDataFilePM_ =getParam<std::string>("Problem.RelativeHumidityDataFile");
            readData(relativeHumidityDataFilePM_, relativeHumidityDataPM_);

            // for laminar flow
            //boundaryLayerDataFilePM_ =getParam<std::string>("Problem.BoundaryLayerDataFile");
            //readData(boundaryLayerDataFilePM_, boundaryLayerDataPM_);

            //for laminar/turbulent flow
            windSpeedDataFilePM_ =getParam<std::string>("Problem.WindSpeedDataFile");
            readData(windSpeedDataFilePM_, windSpeedDataPM_);

            radiationDataFilePM_ =getParam<std::string>("Problem.RadiationDataFile");
            readData(radiationDataFilePM_, radiationDataPM_);
        }

        void setTime(Scalar time)
        { time_ = time; }

        const Scalar time() const
        {return time_; }

        void setPreviousTime(Scalar time)
        { previousTime_ = time; }

        const Scalar previousTime() const
        {return previousTime_; }

        //! Called after every time step
        //! Output the total global exchange term
        void postTimeStep(const SolutionVector& curSol,
                          const GridVariables &gridVariables)
        {
            Scalar evaporation = 0.0;
            Scalar netRadiation = 0.0;
            Scalar boundaryLayerThicknessPlot =0.0;

            if (!(time() < 0.0))
            {
                Scalar refTemperature = 273.15 + evaluateData(temperatureDataPM_, previousTime(), time());

                for (const auto& element : elements(this->fvGridGeometry().gridView()))
                {
                    auto fvGeometry = localView(this->fvGridGeometry());
                    fvGeometry.bindElement(element);

                    auto elemVolVars = localView(gridVariables.curGridVolVars());
                    elemVolVars.bindElement(element, fvGeometry, curSol);

                    for (auto&& scvf : scvfs(fvGeometry))
                        if (scvf.boundary())
                        {
                            evaporation += neumann(element, fvGeometry, elemVolVars, scvf)[Indices::conti0EqIdx]
                            * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();
                            const auto radiation = Radiation::radationEquilibrium(element, elemVolVars[scvf.insideScvIdx()], scvf,  refTemperature, time());
                            netRadiation += radiation * scvf.area() * elemVolVars[scvf.insideScvIdx()].extrusionFactor();
                            const auto& volVars = elemVolVars[scvf.insideScvIdx()];

                            Scalar WindSpeedPlot = evaluateData(windSpeedDataPM_, previousTime(), time());
                            double RePlot = WindSpeedPlot * 2.0 * volVars.density(FluidSystem::gasPhaseIdx) / volVars.viscosity(FluidSystem::gasPhaseIdx);

                            if (RePlot < 5*1e5) { //laminar flow
                                using std::sqrt;
                                boundaryLayerThicknessPlot = 5.0*sqrt(volVars.viscosity(FluidSystem::gasPhaseIdx)*distance_ / (0.99*WindSpeedPlot*volVars.density(FluidSystem::gasPhaseIdx)));
                            }

                            else {
                                Scalar RexPlot = WindSpeedPlot * distance_ * volVars.density(FluidSystem::gasPhaseIdx) / volVars.viscosity(FluidSystem::gasPhaseIdx);
                                using std::pow;
                                boundaryLayerThicknessPlot = 0.16 * distance_ / pow(RexPlot, 1/7);
                            }
                        }
                }
            }
            // convert to kg/s if using mole fractions
            evaporation = useMoles ? evaporation * FluidSystem::molarMass(FluidSystem::H2OIdx) : evaporation;
            std::cout << "Soil evaporation rate: " << evaporation << " kg/s." << '\n';
            //do a gnuplot
            x_.push_back(time()/3600); // in seconds
            y_.push_back(evaporation);

            gnuplot_.resetPlot();
            gnuplot_.setXRange(0,std::max(time()/3600, 24.0));
            gnuplot_.setYRange(0, 4e-5);
            gnuplot_.setXlabel("time [h]");
            gnuplot_.setYlabel("kg/s");
            gnuplot_.addDataSetToPlot(x_, y_, "evaporation_Sb10");
            gnuplot_.plot("evaporation rate");

            //do a gnuplot
            y4_.push_back(netRadiation);
            gnuplot4_.resetPlot();
            gnuplot4_.setXRange(0,std::max(time()/3600, 24.0));
            gnuplot4_.setYRange(-100, 1500);
            gnuplot4_.setXlabel("time [s]");
            gnuplot4_.setYlabel("W/m^2");
            gnuplot4_.addDataSetToPlot(x_, y4_, "net radiation");
            //gnuplot4_.plot("radiation");

            //do a gnuplot
            y5_.push_back(boundaryLayerThicknessPlot);
            gnuplot5_.resetPlot();
            gnuplot5_.setXRange(0,std::max(time()/3600, 24.0));
            gnuplot5_.setYRange(0, 0.2);
            gnuplot5_.setXlabel("time [s]");
            gnuplot5_.setYlabel("m");
            gnuplot5_.addDataSetToPlot(x_, y5_, "Boundarylayer tickness");
            gnuplot5_.plot("Boundarylayer thickness");
        }

        /*!
         * \name Problem parameters
         */
        // \{

        /*!
         * \brief Returns the problem name
         *
         * This is used as a prefix for files generated by the simulation.
         */
        const std::string& name() const
        { return name_; }

        /*!
         * \brief Returns the temperature [K]
         */
        Scalar temperature() const
        { return 293.15; }

        // \}

        /*!
         * \name Boundary conditions
         */
        // \{

        /*!
         * \brief Specifies which kind of boundary condition should be
         *        used for which equation on a given boundary segment
         *
         * \param globalPos The global position
         */
        BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
        {
            BoundaryTypes bcTypes;

            bcTypes.setAllNeumann();

            // if(globalPos[0] < this->fvGridGeometry().bBoxMin()[0]+ eps_)
            //   bcTypes.setAllDirichlet();

            return bcTypes;
        }

        /*!
         * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
         *
         * \param globalPos The global position
         */
        PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
        {
            PrimaryVariables priVars;
            priVars.setState(Indices::bothPhases);
            priVars[Indices::pressureIdx] = 1e5;
            //TODO: if soil is switched, change inital soil moisture content
            priVars[Indices::switchIdx] = 0.92;
            priVars[Indices::temperatureIdx] = temperatureInitial_;
            return priVars;
        }

        /*!
         * \brief Evaluates the boundary conditions for a Neumann boundary segment.
         *
         * For this method, the \a priVars parameter stores the mass flux
         * in normal direction of each component. Negative values mean influx.
         * \param globalPos The global position
         *
         * \note The units must be according to either using mole or mass fractions (mole/(m^2*s) or kg/(m^2*s)).
         */

        NumEqVector neumann(const Element &element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolumeFace& scvf) const
                            {
                                NumEqVector values(0.0);
                                const auto globalPos = scvf.ipGlobal();
                                const auto& volVars = elemVolVars[scvf.insideScvIdx()];

                                // BoundaryLayerThickness only laminar per InputFile
                                // Scalar boundaryLayerThickness = evaluateData(boundaryLayerDataPM_, previousTime(), time());

                                //BoundaryLayerThickness denpending on laminar/turbulent Flow
                                //Define Reynoldsnumber

                                Scalar WindSpeed = evaluateData(windSpeedDataPM_, previousTime(), time());
                                double Re = WindSpeed * 2.0 * volVars.density(FluidSystem::gasPhaseIdx) / volVars.viscosity(FluidSystem::gasPhaseIdx);
                                using std::log;

                                Scalar boundaryLayerThickness;

                                // Criterium forturbulent flow
                                // Re >= 5*1e5 (Schlichting, 2017; White, 2011)
                                // Re >= 3*1e5 strong pertubation (Schlichting, 2017)
                                // Re >= 3*e6 smooth flow (Schlichting, 2017; White, 2011)

                                if (Re < 5*1e5) { //laminar flow


                                    using std::sqrt;

                                    boundaryLayerThickness = 5.0*sqrt(volVars.viscosity(FluidSystem::gasPhaseIdx)*distance_ / (0.99*WindSpeed*volVars.density(FluidSystem::gasPhaseIdx)));
                                }

                                else {

                                    Scalar Rex = WindSpeed * distance_ * volVars.density(FluidSystem::gasPhaseIdx) / volVars.viscosity(FluidSystem::gasPhaseIdx);
                                    //turbulent flow (White, 2011)
                                    using std::pow;
                                    boundaryLayerThickness = 0.16 * distance_ / pow(Rex, (1/7));

                                }

                                Scalar refTemperature = 273.15 + evaluateData(temperatureDataPM_, previousTime(), time());

                                //relativeHumidity/100 to convert from percent
                                Scalar refMoleFracVapor = evaluateData(relativeHumidityDataPM_, previousTime(), time())/100*H2O::vaporPressure(refTemperature)/1e5;
                                Scalar moleFracH2OInside = volVars.moleFraction(FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx);

                                if(globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_)
                                {
                                    // calculate fluxes
                                    Scalar evaporationRateMole = volVars.diffusionCoefficient(FluidSystem::gasPhaseIdx, FluidSystem::H2OIdx)
                                    * (moleFracH2OInside - refMoleFracVapor)
                                    / boundaryLayerThickness
                                    * volVars.molarDensity(FluidSystem::gasPhaseIdx);

                                    values[Indices::conti0EqIdx] = evaporationRateMole;
                                    values[Indices::conti0EqIdx+1] = -evaporationRateMole;

                                    if (volVars.pressure(FluidSystem::gasPhaseIdx) - 1e5 > 0) {
                                        values[Indices::conti0EqIdx+1] = (volVars.pressure(FluidSystem::gasPhaseIdx) - 1e5)
                                        /(globalPos - fvGeometry.scv(scvf.insideScvIdx()).center()).two_norm()
                                        *volVars.mobility(FluidSystem::gasPhaseIdx)
                                        *this->spatialParams().permeabilityAtPos(globalPos)
                                        *volVars.molarDensity(FluidSystem::gasPhaseIdx) * volVars.moleFraction(FluidSystem::gasPhaseIdx, FluidSystem::N2Idx);
                                    }
                                    else {
                                        values[Indices::conti0EqIdx+1] = (volVars.pressure(FluidSystem::gasPhaseIdx) - 1e5)
                                        /(globalPos - fvGeometry.scv(scvf.insideScvIdx()).center()).two_norm()
                                        *volVars.mobility(FluidSystem::gasPhaseIdx)
                                        *this->spatialParams().permeabilityAtPos(globalPos)
                                        *volVars.molarDensity(FluidSystem::gasPhaseIdx) * (1-refMoleFracVapor);
                                    }

                                    values[Indices::energyEqIdx] = H2O::gasEnthalpy(volVars.temperature(),volVars.pressure(FluidSystem::gasPhaseIdx)) * values[Indices::conti0EqIdx]*FluidSystem::molarMass(FluidSystem::H2OIdx);

                                    values[Indices::energyEqIdx] = Air::gasEnthalpy(refTemperature,1e5) * values[Indices::conti0EqIdx+1]*FluidSystem::molarMass(FluidSystem::N2Idx);

                                    values[Indices::energyEqIdx] += Air::gasThermalConductivity(refTemperature, 1)* (volVars.temperature() - refTemperature)/boundaryLayerThickness;

                                    //get radiation from input data
                                    values[Indices::energyEqIdx] += -1*evaluateData(radiationDataPM_, previousTime(), time());

                                    //get calculated radiation
                                    // values[Indices::energyEqIdx] += -1*Radiation::radationEquilibrium(element, elemVolVars[scvf.insideScvIdx()], scvf,  refTemperature, time());
                                }

                                return values;

                            }


                            // \}

                            /*!
                             * \name Volume terms
                             */
                            // \{

                            /*!
                             * \brief Evaluates the initial values for a control volume.
                             *
                             * \param globalPos The global position
                             */
                            PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
                            {
                                return initial_(globalPos);
                            }

                            // \}

    private:
        /*!
         * \brief Evaluates the initial values for a control volume.
         *
         * The internal method for the initial condition
         *
         * \param globalPos The global position
         */
        PrimaryVariables initial_(const GlobalPosition &globalPos) const
        {
            PrimaryVariables priVars(0.0);
            priVars.setState(Indices::bothPhases);

            //mole-fraction formulation
            //TODO: if soil is switched, change initial soil moisture content and initial temperature

            if (globalPos[0] < 0.01 - eps_)
                priVars[Indices::switchIdx] =  0.92;
            else if (globalPos[0] > .01 + eps_ && globalPos[0] < 0.29 - eps_)
                priVars[Indices::switchIdx] =  0.89;
            else if (globalPos[0] > 0.29 + eps_ && globalPos[0] < .86 - eps_)
               priVars[Indices::switchIdx] =  0.88;
            else
                priVars[Indices::switchIdx] =  0.91;

            priVars[Indices::pressureIdx] = 1e5;

            if (globalPos[0] < 0.9 - eps_)
                priVars[Indices::temperatureIdx] = 273.15 + 18.4;
            else if (globalPos[0] > .90 + eps_ && globalPos[0] < 1.1 - eps_)
                priVars[Indices::temperatureIdx] = 273.15 + 19.9;
            else if (globalPos[0] > 1.1 + eps_ && globalPos[0] < 1.4 - eps_)
                priVars[Indices::temperatureIdx] = 273.15 + 20.1;
            else
                priVars[Indices::temperatureIdx] = 273.15 + 18.4;
            return priVars;
        }

        Scalar temperature_;
        static constexpr Scalar eps_ = 1e-6;
        std::string name_ ;
        Scalar temperatureInitial_;

        Scalar time_;
        Scalar previousTime_;
        Scalar distance_ = 20; // [m]  distance downstream from start of the boundary layer; free variable --> strongly influences the results.


        std::string temperatureDataFilePM_;
        std::vector<double> temperatureDataPM_[2];
        std::string relativeHumidityDataFilePM_;
        std::vector<double> relativeHumidityDataPM_[2];

        // for laminar flow
        //std::string boundaryLayerDataFilePM_;
        //std::vector<double> boundaryLayerDataPM_[2];

        // for laminar/turbulent flow
        std::string windSpeedDataFilePM_;
        std::vector<double> windSpeedDataPM_[2];

        std::string radiationDataFilePM_;
        std::vector<double> radiationDataPM_[2];


        Dumux::GnuplotInterface<double> gnuplot_;
        Dumux::GnuplotInterface<double> gnuplot4_;
        Dumux::GnuplotInterface<double> gnuplot5_;


        std::vector<double> x_;
        std::vector<double> y_;
        std::vector<double> y4_;
        std::vector<double> y5_;


    };

} // end namespace Dumux

#endif
