// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPTwoCModel
 * \brief Contains the quantities which are constant within a
 *        finite volume in the two-phase two-component model.
 */

#ifndef DUMUX_ISOTOPE_VOLUME_VARIABLES_HH
#define DUMUX_ISOTOPE_VOLUME_VARIABLES_HH

#include <dumux/material/fluidstates/compositional.hh>
#include <dumux/material/constraintsolvers/computefromreferencephase.hh>
#include <dumux/material/constraintsolvers/misciblemultiphasecomposition.hh>

#include <dumux/discretization/method.hh>
#include <dumux/porousmediumflow/volumevariables.hh>
#include <dumux/porousmediumflow/nonisothermal/volumevariables.hh>
#include <dumux/porousmediumflow/2p/formulation.hh>
#include <dumux/material/solidstates/updatesolidvolumefractions.hh>
#include <dumux/porousmediumflow/2pnc/primaryvariableswitch.hh>
#include <cmath>

namespace Dumux {

/*!
 * \ingroup TwoPTwoCModel
 * \brief Contains the quantities which are constant within a
 *        finite volume in the two-phase two-component model.
 */
template <class Traits>
class TwoPIsotopeVolumeVariables
: public PorousMediumFlowVolumeVariables<Traits>
, public EnergyVolumeVariables<Traits, TwoPIsotopeVolumeVariables<Traits> >
{
    using ParentType = PorousMediumFlowVolumeVariables< Traits >;
    using EnergyVolVars = EnergyVolumeVariables<Traits, TwoPIsotopeVolumeVariables<Traits> >;

    using Scalar = typename Traits::PrimaryVariables::value_type;
    using ModelTraits = typename Traits::ModelTraits;

    static constexpr int numFluidComps = ParentType::numFluidComponents();
    // component indices
    enum
    {
        comp0Idx = Traits::FluidSystem::comp0Idx,
        comp1Idx = Traits::FluidSystem::comp1Idx,
        phase0Idx = Traits::FluidSystem::phase0Idx,
        phase1Idx = Traits::FluidSystem::phase1Idx,
        numMajorComponents = ModelTraits::numFluidPhases(),
        numIsotopes = numFluidComps-numMajorComponents,
    };

    // phase presence indices
    enum
    {
        firstPhaseOnly = ModelTraits::Indices::firstPhaseOnly,
        secondPhaseOnly = ModelTraits::Indices::secondPhaseOnly,
        bothPhases = ModelTraits::Indices::bothPhases
    };

    // primary variable indices
    enum
    {
        switchIdx = ModelTraits::Indices::switchIdx,
        pressureIdx = ModelTraits::Indices::pressureIdx
    };

    // formulations
    static constexpr auto formulation = ModelTraits::priVarFormulation();

    using PermeabilityType = typename Traits::PermeabilityType;
    static constexpr bool setFirstPhaseMoleFractions = ModelTraits::setMoleFractionsForFirstPhase();

public:
    //! The type of the object returned by the fluidState() method
    using FluidState = typename Traits::FluidState;
    //! The fluid system used here
    using FluidSystem = typename Traits::FluidSystem;
    //! Export type of solid state
    using SolidState = typename Traits::SolidState;
    //! Export type of solid system
    using SolidSystem = typename Traits::SolidSystem;
    //! Export the primary variable switch
    using PrimaryVariableSwitch = TwoPNCPrimaryVariableSwitch;

    //! Return whether moles or masses are balanced
    static constexpr bool useMoles() { return ModelTraits::useMoles(); }
    //! Return the two-phase formulation used here
    static constexpr TwoPFormulation priVarFormulation() { return formulation; }

    static_assert(ModelTraits::numFluidPhases() == 2, "NumPhases set in the model is not two!");
    static_assert((formulation == TwoPFormulation::p0s1 || formulation == TwoPFormulation::p1s0), "Chosen TwoPFormulation not supported!");

    /*!
     * \brief Updates all quantities for a given control volume.
     *
     * \param elemSol A vector containing all primary variables connected to the element
     * \param problem The object specifying the problem which ought to
     *                be simulated
     * \param element An element which contains part of the control volume
     * \param scv The sub control volume
    */
    template<class ElemSol, class Problem, class Element, class Scv>
    void update(const ElemSol& elemSol, const Problem& problem, const Element& element, const Scv& scv)
    {
        ParentType::update(elemSol, problem, element, scv);
        completeFluidState(elemSol, problem, element, scv, fluidState_, solidState_);

        // Second instance of a parameter cache. Could be avoided if
        // diffusion coefficients also became part of the fluid state.
        typename FluidSystem::ParameterCache paramCache;
        paramCache.updateAll(fluidState_);

        using MaterialLaw = typename Problem::SpatialParams::MaterialLaw;
        const auto& matParams = problem.spatialParams().materialLawParams(element, scv, elemSol);

        const int wPhaseIdx = problem.spatialParams().template wettingPhase<FluidSystem>(element, scv, elemSol);
        const int nPhaseIdx = 1 - wPhaseIdx;

        // relative permeabilities -> require wetting phase saturation as parameter!
        relativePermeability_[wPhaseIdx] = MaterialLaw::krw(matParams, saturation(wPhaseIdx));
        relativePermeability_[nPhaseIdx] = MaterialLaw::krn(matParams, saturation(wPhaseIdx));

        // binary diffusion coefficients
        for (unsigned int compJIdx = 0; compJIdx <  ModelTraits::numFluidComponents(); ++compJIdx)
        {
            if(compJIdx != comp0Idx)
                setDiffusionCoefficient_( phase0Idx, compJIdx,
                                        FluidSystem::binaryDiffusionCoefficient(fluidState_,
                                                                                paramCache,
                                                                                phase0Idx,
                                                                                comp0Idx,
                                                                                compJIdx) );
            if(compJIdx != comp1Idx)
                setDiffusionCoefficient_( phase1Idx, compJIdx,
                                        FluidSystem::binaryDiffusionCoefficient(fluidState_,
                                                                                paramCache,
                                                                                phase1Idx,
                                                                                comp1Idx,
                                                                                compJIdx) );
        }
        // porosity & permeabilty
        updateSolidVolumeFractions(elemSol, problem, element, scv, solidState_, numFluidComps);
        EnergyVolVars::updateSolidEnergyParams(elemSol, problem, element, scv, solidState_);
        permeability_ = problem.spatialParams().permeability(element, scv, elemSol);
    }

    /*!
     * \brief Sets complete fluid state.
     *
     * \param elemSol A vector containing all primary variables connected to the element
     * \param problem The object specifying the problem which ought to
     *                be simulated
     * \param element An element which contains part of the control volume
     * \param scv The sub-control volume
     * \param fluidState A container with the current (physical) state of the fluid
     * \param solidState A container with the current (physical) state of the solid
     *
     * Set temperature, saturations, capillary pressures, viscosities, densities and enthalpies.
     */
    template<class ElemSol, class Problem, class Element, class Scv>
    void completeFluidState(const ElemSol& elemSol,
                            const Problem& problem,
                            const Element& element,
                            const Scv& scv,
                            FluidState& fluidState,
                            SolidState& solidState)
    {
        EnergyVolVars::updateTemperature(elemSol, problem, element, scv, fluidState, solidState);

        const auto& priVars = elemSol[scv.localDofIndex()];
        const auto phasePresence = priVars.state();

        using MaterialLaw = typename Problem::SpatialParams::MaterialLaw;
        const auto& materialParams = problem.spatialParams().materialLawParams(element, scv, elemSol);
        const int wPhaseIdx = problem.spatialParams().template wettingPhase<FluidSystem>(element, scv, elemSol);
        fluidState.setWettingPhase(wPhaseIdx);

        // set the saturations
        if (phasePresence == firstPhaseOnly)
        {
            fluidState.setSaturation(phase0Idx, 1.0);
            fluidState.setSaturation(phase1Idx, 0.0);
        }
        else if (phasePresence == secondPhaseOnly)
        {
            fluidState.setSaturation(phase0Idx, 0.0);
            fluidState.setSaturation(phase1Idx, 1.0);
        }
        else if (phasePresence == bothPhases)
        {
            if (formulation == TwoPFormulation::p0s1)
            {
                fluidState.setSaturation(phase1Idx, priVars[switchIdx]);
                fluidState.setSaturation(phase0Idx, 1 - priVars[switchIdx]);
            }
            else
            {
                fluidState.setSaturation(phase0Idx, priVars[switchIdx]);
                fluidState.setSaturation(phase1Idx, 1 - priVars[switchIdx]);
            }
        }
        else
            DUNE_THROW(Dune::InvalidStateException, "Invalid phase presence.");

        // set pressures of the fluid phases
        pc_ = MaterialLaw::pc(materialParams, fluidState.saturation(wPhaseIdx));
        if (formulation == TwoPFormulation::p0s1)
        {
            fluidState.setPressure(phase0Idx, priVars[pressureIdx]);
            fluidState.setPressure(phase1Idx, (wPhaseIdx == phase0Idx) ? priVars[pressureIdx] + pc_
                                                                       : priVars[pressureIdx] - pc_);
        }
        else
        {
            fluidState.setPressure(phase1Idx, priVars[pressureIdx]);
            fluidState.setPressure(phase0Idx, (wPhaseIdx == phase0Idx) ? priVars[pressureIdx] - pc_
                                                                       : priVars[pressureIdx] + pc_);
        }

        // calculate the phase compositions
        typename FluidSystem::ParameterCache paramCache;


        for (int phaseIdx = 0; phaseIdx < ModelTraits::numFluidPhases(); ++ phaseIdx)
        {
            assert(FluidSystem::isIdealMixture(phaseIdx));
            for (int compIdx = 0; compIdx < ModelTraits::numFluidComponents()-1; ++ compIdx) {
                Scalar phi = FluidSystem::fugacityCoefficient(fluidState, paramCache, phaseIdx, compIdx);
                fluidState.setFugacityCoefficient(phaseIdx, compIdx, phi);
            }
        }

        // now comes the tricky part: calculate phase compositions
        const Scalar p0 = fluidState.pressure(phase0Idx);
//         const Scalar p1 = fluidState.pressure(phase1Idx); //prüfe mit 1e5
        const Scalar p1 = 1e5; //prüfe mit 1e5

        using std::exp;
        Dune::FieldVector<Scalar, numIsotopes> alpha(0.0);
        for (int compIdx = 0; compIdx <  numIsotopes; ++compIdx)
        {
            if (compIdx == 0)
                alpha[compIdx] = exp(-((24844.0/(temperature()*temperature()))+(-76.248/temperature())+0.052612));
            else 
                alpha[compIdx] = exp(-((1137.0/(temperature()*temperature()))+(-0.4156/temperature())-0.0020667));
        }
// std::cout<<"alphavol"<<alpha[1]<<std::endl;
        if (phasePresence == bothPhases)
        {
            // set known and unknown phase indices
            const int knownPhaseIdx = setFirstPhaseMoleFractions ? phase0Idx : phase1Idx;
            const int unknownPhaseIdx = setFirstPhaseMoleFractions ? phase1Idx : phase0Idx;
            
   
            

            // set the mole fraction for hdo in the known phase
            Dune::FieldVector<Scalar, numIsotopes> xKnownIsotope(0.0);
            //only if we have more components than phases there is a primary variable for the isotope defined
           for (int compIdx = 0; compIdx <  numIsotopes; ++compIdx)
                xKnownIsotope[compIdx] += priVars[numMajorComponents+compIdx];
           


            Dune::FieldVector<Scalar, numIsotopes> xUnknownIsotope(0.0);
            Dune::FieldVector<Scalar, numIsotopes> partPressureVaporIsotope(0.0);
            if (setFirstPhaseMoleFractions)
            {
                // if we know the wetting phase, the unknown mole fraction is the mole fraction of hdo in the gas phase.
                // We use the fractioning coefficient to find the mole fraction of isotope in the gas phase (x_n^hdo = x_w^hdo * alpha) (Braud et al 2004)
                // The parial pressure due to the isotope is equal to the mole fraction * gas pressure.
                

                
                
                for (int compIdx = 0; compIdx <  numIsotopes; ++compIdx)
                {   
                    
                                                        // get the partial pressure of the main component of the first phase within the
            // second phase == vapor pressure due to equilibrium. Note that in this case the
            // fugacityCoefficient * p is the vapor pressure (see implementation in respective fluidsystem)
            const Scalar partPressLiquid = FluidSystem::fugacityCoefficient(fluidState, phase0Idx, comp0Idx)*p0;

            // calculate the mole fractions of the components within the nonwetting phase
            // partPressLiquid is the partial pressure from both hdo and water. subtract the isotopes partial pressure to get the partial pressure only from the water
            Scalar partPressureH2O = partPressLiquid;
            for (int compIdx = 0; compIdx < numIsotopes; ++compIdx)
                partPressureH2O += -partPressureVaporIsotope[compIdx];

            const Scalar xnw = partPressureH2O / p1;
                    
                    
                    
                    xUnknownIsotope[compIdx] += xKnownIsotope[compIdx]*alpha[compIdx]*xnw;
                    partPressureVaporIsotope[compIdx] += xUnknownIsotope[compIdx]*p1;
                }
            }
            else
            {
                // if we know the nonwetting phase, the unknown mole fraction is the mole fraction of hdo in the water phase.
                // We use the fractioning coefficient to find the mole fraction of isotope in the water phase (x_w^hdo = x_n^hdo / alpha) (Braud et al 2004)
                // The parial pressure due to the isotope is equal to the mole fraction * gas pressure.
                for (int compIdx = 0; compIdx <  numIsotopes; ++compIdx)
                {
                                                        // get the partial pressure of the main component of the first phase within the
            // second phase == vapor pressure due to equilibrium. Note that in this case the
            // fugacityCoefficient * p is the vapor pressure (see implementation in respective fluidsystem)
            const Scalar partPressLiquid = FluidSystem::fugacityCoefficient(fluidState, phase0Idx, comp0Idx)*p0;

            // calculate the mole fractions of the components within the nonwetting phase
            // partPressLiquid is the partial pressure from both hdo and water. subtract the isotopes partial pressure to get the partial pressure only from the water
            Scalar partPressureH2O = partPressLiquid;
            for (int compIdx = 0; compIdx < numIsotopes; ++compIdx)
                partPressureH2O += -partPressureVaporIsotope[compIdx];

            const Scalar xnw = partPressureH2O / p1;
                    
                    
                    xUnknownIsotope[compIdx] += xKnownIsotope[compIdx]/alpha[compIdx]/xnw ;
                    partPressureVaporIsotope[compIdx] += xKnownIsotope[compIdx]*p1;
                }
            }

                    // get the partial pressure of the main component of the first phase within the
            // second phase == vapor pressure due to equilibrium. Note that in this case the
            // fugacityCoefficient * p is the vapor pressure (see implementation in respective fluidsystem)
            const Scalar partPressLiquid = FluidSystem::fugacityCoefficient(fluidState, phase0Idx, comp0Idx)*p0;

            // calculate the mole fractions of the components within the nonwetting phase
            // partPressLiquid is the partial pressure from both hdo and water. subtract the isotopes partial pressure to get the partial pressure only from the water
            Scalar partPressureH2O = partPressLiquid;
            for (int compIdx = 0; compIdx < numIsotopes; ++compIdx)
                partPressureH2O += -partPressureVaporIsotope[compIdx];

            const Scalar xnw = partPressureH2O / p1;

            // get the partial pressure of the main component of the gas phase
            // this is daltons law. sum of partial pressures is total pressure
            const Scalar partPressGas = p1 - partPressLiquid;
            const Scalar xnn = partPressGas / p1;

            // calculate the mole fractions of the components within the wetting phase
            // note that in this case the fugacityCoefficient * p is the Henry Coefficient
            // (see implementation in respective fluidsystem)
            const Scalar xwn = partPressGas / (FluidSystem::fugacityCoefficient(fluidState, phase0Idx, comp1Idx)*p0);
            Scalar xww = 1.0-xwn;
            if (setFirstPhaseMoleFractions)
                for (int compIdx = 0; compIdx < numIsotopes; ++compIdx)
                    xww +=- xKnownIsotope[compIdx];
            else
                for (int compIdx = 0; compIdx < numIsotopes; ++compIdx)
                    xww +=- xUnknownIsotope[compIdx];

            // set all mole fractions
            fluidState.setMoleFraction(phase0Idx, comp0Idx, xww);
            fluidState.setMoleFraction(phase0Idx, comp1Idx, xwn);
            fluidState.setMoleFraction(phase1Idx, comp0Idx, xnw);
            fluidState.setMoleFraction(phase1Idx, comp1Idx, xnn);
            for (int compIdx = 0; compIdx < numIsotopes; ++compIdx)
            {
                fluidState.setMoleFraction(knownPhaseIdx, numMajorComponents+compIdx, xKnownIsotope[compIdx]);
                fluidState.setMoleFraction(unknownPhaseIdx, numMajorComponents+compIdx, xUnknownIsotope[compIdx]);
            }
        }
        else if (phasePresence == secondPhaseOnly)
        {
            // there is a primary variable for the water component defined (excluding hdo)
            Scalar sumMoleFracOtherComponents = priVars[switchIdx];

            Dune::FieldVector<Scalar, numIsotopes> xnIsotope(0.0);
            // here, only the second phase is present, so hdo composition is stored explicitly.
            for (int compIdx = 0; compIdx <  numIsotopes; ++compIdx)
            {
                xnIsotope[compIdx] += priVars[numMajorComponents+compIdx];
                sumMoleFracOtherComponents += xnIsotope[compIdx];
            }

            const Scalar xnw = priVars[switchIdx];
            const Scalar xnn = 1.0 - sumMoleFracOtherComponents;

            // set the mole fractions for the nonwetting phase
            fluidState.setMoleFraction(phase1Idx, comp0Idx, xnw);
            fluidState.setMoleFraction(phase1Idx, comp1Idx, xnn);
            for (int compIdx = 0; compIdx <  numIsotopes; ++compIdx)
                fluidState.setMoleFraction(phase1Idx, numMajorComponents+compIdx, xnIsotope[compIdx]);

            // note that the water phase does not really exist!
            // thus, this is used as phase switch criterion

            // first, xww:
            // xnw * pn = "actual" (hypothetical) vapor pressure
            // fugacityCoefficient * pw = vapor pressure given by thermodynamic conditions
            // Here, xww is not actually the mole fraction of water in the wetting phase
            // xww is only the ratio of "actual" vapor pressure / "thermodynamic" vapor pressure
            // If the sum of all components in the water phase is more than 1 : gas is over-saturated with water vapor,
            // condensation takes place (see switch criterion in model)
            Scalar xww = xnw*p1/( FluidSystem::fugacityCoefficient(fluidState, phase0Idx, comp0Idx)*p0 );
            // second, xwn:
            // partialPressure / xwn = Henry
            // partialPressure = xnn * pn
            // xwn = xnn * pn / Henry
            // Henry = fugacityCoefficient * pw
            const Scalar xwn = xnn*p1/( FluidSystem::fugacityCoefficient(fluidState, phase0Idx, comp1Idx)*p0 );
            // Use the fractioning coefficient to find the isotopes mole fraction
            // x_w^hdo = x_n^hdo / alpha (Braud et al 2004)
            Dune::FieldVector<Scalar, numIsotopes> xwIsotope(0.0);
            for (int compIdx = 0; compIdx <  numIsotopes; ++compIdx)
            {
                xwIsotope[compIdx] = xnIsotope[compIdx]/alpha[compIdx]/xnw ;
                xww += -xwIsotope[compIdx];
            }
            // set the mole fractions for the wetting phase
            fluidState.setMoleFraction(phase0Idx, comp0Idx, xww);
            fluidState.setMoleFraction(phase0Idx, comp1Idx, xwn);
            for (int compIdx = 0; compIdx <  numIsotopes; ++compIdx)
                fluidState.setMoleFraction(phase0Idx, numMajorComponents+compIdx, xwIsotope[compIdx]);
        }
        else if (phasePresence == firstPhaseOnly)
        {
            // only the wetting phase is present, i.e. wetting phase
            // composition is stored explicitly.
            Scalar sumMoleFracOtherComponents = priVars[switchIdx];

            Dune::FieldVector<Scalar, numIsotopes> xwIsotope(0.0);
            for (int compIdx = 0; compIdx <  numIsotopes; ++compIdx)
            {
                xwIsotope[compIdx] += priVars[numMajorComponents+compIdx];
                sumMoleFracOtherComponents += xwIsotope[compIdx];
            }

            const Scalar xwn = priVars[switchIdx];
            const Scalar xww = 1.0 - sumMoleFracOtherComponents;

            // set the mole fractions for the wetting phase
            fluidState.setMoleFraction(phase0Idx, comp0Idx, xwn);
            fluidState.setMoleFraction(phase0Idx, comp1Idx, xww);
            for (int compIdx = 0; compIdx <  numIsotopes; ++compIdx)
                fluidState.setMoleFraction(phase0Idx, numMajorComponents+compIdx, xwIsotope[compIdx]);

            // note that the gas phase does not really exist!
            // thus, this is used as phase switch criterion

            // first, xnw:
            // psteam = xnw * pn = partial pressure of water in gas phase
            // psteam = fugacityCoefficient * pw
            Scalar xnw = ( FluidSystem::fugacityCoefficient(fluidState, phase0Idx, comp0Idx) * p0) / p1;

            // second, xnn:
            // xwn = partialPressure / Henry
            // partialPressure = pn * xnn
            // xwn = pn * xnn / Henry
            // xnn = xwn * Henry / pn
            // Henry = fugacityCoefficient * pw
            const Scalar xnn = xwn * ( FluidSystem::fugacityCoefficient(fluidState, phase0Idx, comp1Idx) * p0) / p1;

            // Use the fractioning coefficient to find the isotopes mole fraction
            // x_n^hdo = x_w^hdo * alpha (Braud et al 2004)
            Dune::FieldVector<Scalar, numIsotopes> xnIsotope(0.0);
            for (int compIdx = 0; compIdx <  numIsotopes; ++compIdx)
            {
                xnIsotope[compIdx] = xwIsotope[compIdx]*alpha[compIdx]*xnw ;
                xnw += -xnIsotope[compIdx];
            }
            // set the mole fractions for the nonwetting phase
            fluidState.setMoleFraction(phase1Idx, comp0Idx, xnw);
            fluidState.setMoleFraction(phase1Idx, comp1Idx, xnn);
            for (int compIdx = 0; compIdx <  numIsotopes; ++compIdx)
                fluidState.setMoleFraction(phase1Idx, numMajorComponents+compIdx, xnIsotope[compIdx]);
        }

        for (int phaseIdx = 0; phaseIdx < ModelTraits::numFluidPhases(); ++phaseIdx)
        {
            paramCache.updateComposition(fluidState, phaseIdx);
            const Scalar rho = FluidSystem::density(fluidState, paramCache, phaseIdx);
            fluidState.setDensity(phaseIdx, rho);
            Scalar rhoMolar = FluidSystem::molarDensity(fluidState, paramCache, phaseIdx);
            fluidState.setMolarDensity(phaseIdx, rhoMolar);

            // compute and set the enthalpy
            const Scalar mu = FluidSystem::viscosity(fluidState, paramCache, phaseIdx);
            fluidState.setViscosity(phaseIdx,mu);
            Scalar h = EnergyVolVars::enthalpy(fluidState, paramCache, phaseIdx);
            fluidState.setEnthalpy(phaseIdx, h);
        }
    }

    /*!
     * \brief Returns the phase state within the control volume.
     */
    const FluidState &fluidState() const
    { return fluidState_; }

    /*!
     * \brief Returns the phase state for the control-volume.
     */
    const SolidState &solidState() const
    { return solidState_; }

    /*!
     * \brief Returns the saturation of a given phase within
     *        the control volume in \f$[-]\f$.
     *
     * \param phaseIdx The phase index
     */
    Scalar saturation(const int phaseIdx) const
    { return fluidState_.saturation(phaseIdx); }

    /*!
     * \brief Returns the mass fraction of a given component in a
     *        given phase within the control volume in \f$[-]\f$.
     *
     * \param phaseIdx The phase index
     * \param compIdx The component index
     */
    Scalar massFraction(const int phaseIdx, const int compIdx) const
    { return fluidState_.massFraction(phaseIdx, compIdx); }

    /*!
     * \brief Returns the mole fraction of a given component in a
     *        given phase within the control volume in \f$[-]\f$.
     *
     * \param phaseIdx The phase index
     * \param compIdx The component index
     */
    Scalar moleFraction(const int phaseIdx, const int compIdx) const
    { return fluidState_.moleFraction(phaseIdx, compIdx); }

    /*!
     * \brief Returns the mass density of a given phase within the
     *        control volume in \f$[kg/m^3]\f$.
     *
     * \param phaseIdx The phase index
     */
    Scalar density(const int phaseIdx) const
    { return fluidState_.density(phaseIdx); }

    /*!
     * \brief Returns the dynamic viscosity of the fluid within the
     *        control volume in \f$\mathrm{[Pa s]}\f$.
     *
     * \param phaseIdx The phase index
     */
    Scalar viscosity(const int phaseIdx) const
    { return fluidState_.viscosity(phaseIdx); }

    /*!
     * \brief Returns the mass density of a given phase within the
     *        control volume in \f$[mol/m^3]\f$.
     *
     * \param phaseIdx The phase index
     */
    Scalar molarDensity(const int phaseIdx) const
    { return fluidState_.molarDensity(phaseIdx); }

    /*!
     * \brief Returns the effective pressure of a given phase within
     *        the control volume in \f$[kg/(m*s^2)=N/m^2=Pa]\f$.
     *
     * \param phaseIdx The phase index
     */
    Scalar pressure(const int phaseIdx) const
    { return fluidState_.pressure(phaseIdx); }

    /*!
     * \brief Returns temperature within the control volume in \f$[K]\f$.
     *
     * Note that we assume thermodynamic equilibrium, i.e. the
     * temperature of the rock matrix and of all fluid phases are
     * identical.
     */
    Scalar temperature() const
    { return fluidState_.temperature(/*phaseIdx=*/0); }

    /*!
     * \brief Returns the relative permeability of a given phase within
     *        the control volume in \f$[-]\f$.
     *
     * \param phaseIdx The phase index
     */
    Scalar relativePermeability(const int phaseIdx) const
    { return relativePermeability_[phaseIdx]; }

    /*!
     * \brief Returns the effective mobility of a given phase within
     *        the control volume in \f$[s*m/kg]\f$.
     *
     * \param phaseIdx The phase index
     */
    Scalar mobility(const int phaseIdx) const
    { return relativePermeability_[phaseIdx]/fluidState_.viscosity(phaseIdx); }

    /*!
     * \brief Returns the effective capillary pressure within the control volume
     *        in \f$[kg/(m*s^2)=N/m^2=Pa]\f$.
     */
    Scalar capillaryPressure() const
    { return pc_; }

    /*!
     * \brief Returns the average porosity within the control volume in \f$[-]\f$.
     */
    Scalar porosity() const
    { return solidState_.porosity(); }

    /*!
     * \brief Returns the average permeability within the control volume in \f$[m^2]\f$.
     */
    const PermeabilityType& permeability() const
    { return permeability_; }

    /*!
     * \brief Returns the binary diffusion coefficients for a phase in \f$[m^2/s]\f$.
     */
    Scalar diffusionCoefficient(int phaseIdx, int compIdx) const
    {
        if (compIdx < phaseIdx)
            return diffCoefficient_[phaseIdx][compIdx];
        else if (compIdx > phaseIdx)
            return diffCoefficient_[phaseIdx][compIdx-1];
        else
            DUNE_THROW(Dune::InvalidStateException, "Diffusion coefficient called for phaseIdx = compIdx");
    }
    
    
    Scalar alpha (int compIdx) const
    {       using std::exp;
            if (compIdx == 2)
            {
                return exp(-((24844/(temperature()*temperature()))+(-76.248/temperature())+0.052612));
                
            }
            else if (compIdx == 3) 
               return exp(-((1137/(temperature()*temperature()))+(-0.4156/temperature())-0.0020667));
            else 
                DUNE_THROW(Dune::InvalidStateException, "alpha for compIdx doesn't exist");
    }
    
/*
    Scalar deltaNotation(int phaseIdx, int compIdx) const
    {
        if (compIdx == 2)
        {
   
         return  ((fluidState_.moleFraction(phaseIdx, compIdx)/fluidState_.moleFraction(phaseIdx, 0))- (155.76e-6 *alpha(compIdx)))/(155.76e-6 *alpha(compIdx)) *1000;
            
        }
        else if (compIdx == 3)
         
           return  ((fluidState_.moleFraction(phaseIdx, compIdx)/fluidState_.moleFraction(phaseIdx, 0))- (2005.2e-6* alpha (compIdx)))/(2005.2e-6* alpha (compIdx))*1000 ;
        else
            DUNE_THROW(Dune::InvalidStateException, "delta ref for compIdx doesn't exist");
    }
//     */
//     
//     
//     Scalar deltaNotationliquid(int phaseIdx, int compIdx) const
//     {
//         if (compIdx == 2)
//         {
//             return deltaNotation(phaseIdx, compIdx)/ alpha(compIdx);
//         }
//         else if (compIdx == 3)
//             
//             return deltaNotation(phaseIdx, compIdx) / alpha(compIdx);
//         else
//             DUNE_THROW(Dune::InvalidStateException, "delta ref for compIdx doesn't exist");
//     }

    Scalar deltaNotation(int phaseIdx, int compIdx) const //weg damit 
    {
        if (compIdx == 2)
        {
   //Rout_l
         return  (fluidState_.moleFraction(phaseIdx, compIdx)/fluidState_.moleFraction(phaseIdx, 0))/alpha(compIdx);
            
        }
        else if (compIdx == 3)
         
           return  (fluidState_.moleFraction(phaseIdx, compIdx)/fluidState_.moleFraction(phaseIdx, 0))/alpha(compIdx);
        else
            DUNE_THROW(Dune::InvalidStateException, "delta ref for compIdx doesn't exist");
    }
    
    
    
    Scalar deltaNotationliquid(int phaseIdx, int compIdx) const //richtige delta notation 
    {
        if (compIdx == 2)
        {
            return ((fluidState_.moleFraction(phaseIdx, compIdx)-155.76e-6))/155.76e-6*1000.0;
        }
        else if (compIdx == 3)
            
            return ((fluidState_.moleFraction(phaseIdx, compIdx)-2005.2e-6))/2005.2e-6*1000.0;
        else
            DUNE_THROW(Dune::InvalidStateException, "delta ref for compIdx doesn't exist");
    }
    


    Scalar averageMolarMass(const int phaseIdx) const
    {
        return fluidState_.averageMolarMass(phaseIdx);
    }
    
    
    Scalar relativeHumidity() const
    { return fluidState_.relativeHumidity(); }
    
    
private:
    void setDiffusionCoefficient_(int phaseIdx, int compIdx, Scalar d)
    {
        if (compIdx < phaseIdx)
            diffCoefficient_[phaseIdx][compIdx] = std::move(d);
        else if (compIdx > phaseIdx)
            diffCoefficient_[phaseIdx][compIdx-1] = std::move(d);
        else
            DUNE_THROW(Dune::InvalidStateException, "Diffusion coefficient for phaseIdx = compIdx doesn't exist");
    }

    FluidState fluidState_;
    SolidState solidState_;

    Scalar pc_;                     // The capillary pressure
    PermeabilityType permeability_; // Effective permeability within the control volume

    // Relative permeability within the control volume
    std::array<Scalar, ModelTraits::numFluidPhases()> relativePermeability_;

    std::array<std::array<Scalar,  ModelTraits::numFluidComponents()-1>, ModelTraits::numFluidPhases()> diffCoefficient_;

};

} // end namespace Dumux

#endif
